
package ru.argustelecom.jbossws.liragate;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Err" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Strline" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="St" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Tl" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Kkt" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="Td" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Length" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/&gt;
 *         &lt;element name="AdslPortType" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="AdslAnnexType" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="Rs" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Td82" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DopTel" type="{http://www.argustelecom.ru/jbossws/LIRAGate}DopTelRowType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "err",
    "strline",
    "st",
    "tl",
    "kkt",
    "td",
    "length",
    "adslPortType",
    "adslAnnexType",
    "rs",
    "td82",
    "dopTel"
})
@XmlRootElement(name = "GetAvtResponse")
public class GetAvtResponse {

    @XmlElement(name = "Err", required = true)
    protected String err;
    @XmlElement(name = "Strline", required = true)
    protected String strline;
    @XmlElement(name = "St", required = true)
    protected String st;
    @XmlElement(name = "Tl", required = true)
    protected String tl;
    @XmlElement(name = "Kkt")
    protected int kkt;
    @XmlElement(name = "Td")
    protected String td;
    @XmlElement(name = "Length")
    protected Double length;
    @XmlElement(name = "AdslPortType")
    protected Integer adslPortType;
    @XmlElement(name = "AdslAnnexType")
    protected Integer adslAnnexType;
    @XmlElement(name = "Rs")
    protected String rs;
    @XmlElement(name = "Td82")
    protected String td82;
    @XmlElement(name = "DopTel")
    protected List<DopTelRowType> dopTel;

    /**
     * Gets the value of the err property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErr() {
        return err;
    }

    /**
     * Sets the value of the err property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErr(String value) {
        this.err = value;
    }

    /**
     * Gets the value of the strline property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrline() {
        return strline;
    }

    /**
     * Sets the value of the strline property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrline(String value) {
        this.strline = value;
    }

    /**
     * Gets the value of the st property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSt() {
        return st;
    }

    /**
     * Sets the value of the st property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSt(String value) {
        this.st = value;
    }

    /**
     * Gets the value of the tl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTl() {
        return tl;
    }

    /**
     * Sets the value of the tl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTl(String value) {
        this.tl = value;
    }

    /**
     * Gets the value of the kkt property.
     * 
     */
    public int getKkt() {
        return kkt;
    }

    /**
     * Sets the value of the kkt property.
     * 
     */
    public void setKkt(int value) {
        this.kkt = value;
    }

    /**
     * Gets the value of the td property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTd() {
        return td;
    }

    /**
     * Sets the value of the td property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTd(String value) {
        this.td = value;
    }

    /**
     * Gets the value of the length property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getLength() {
        return length;
    }

    /**
     * Sets the value of the length property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setLength(Double value) {
        this.length = value;
    }

    /**
     * Gets the value of the adslPortType property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAdslPortType() {
        return adslPortType;
    }

    /**
     * Sets the value of the adslPortType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAdslPortType(Integer value) {
        this.adslPortType = value;
    }

    /**
     * Gets the value of the adslAnnexType property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAdslAnnexType() {
        return adslAnnexType;
    }

    /**
     * Sets the value of the adslAnnexType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAdslAnnexType(Integer value) {
        this.adslAnnexType = value;
    }

    /**
     * Gets the value of the rs property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRs() {
        return rs;
    }

    /**
     * Sets the value of the rs property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRs(String value) {
        this.rs = value;
    }

    /**
     * Gets the value of the td82 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTd82() {
        return td82;
    }

    /**
     * Sets the value of the td82 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTd82(String value) {
        this.td82 = value;
    }

    /**
     * Gets the value of the dopTel property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dopTel property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDopTel().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DopTelRowType }
     * 
     * 
     */
    public List<DopTelRowType> getDopTel() {
        if (dopTel == null) {
            dopTel = new ArrayList<DopTelRowType>();
        }
        return this.dopTel;
    }

}
