
package ru.argustelecom.jbossws.liragate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Tl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Kkt" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="Rnabn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Nz" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "tl",
    "kkt",
    "rnabn",
    "nz"
})
@XmlRootElement(name = "GetLine")
public class GetLine {

    @XmlElement(name = "Tl")
    protected String tl;
    @XmlElement(name = "Kkt")
    protected Integer kkt;
    @XmlElement(name = "Rnabn")
    protected String rnabn;
    @XmlElement(name = "Nz")
    protected Integer nz;

    /**
     * Gets the value of the tl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTl() {
        return tl;
    }

    /**
     * Sets the value of the tl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTl(String value) {
        this.tl = value;
    }

    /**
     * Gets the value of the kkt property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getKkt() {
        return kkt;
    }

    /**
     * Sets the value of the kkt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setKkt(Integer value) {
        this.kkt = value;
    }

    /**
     * Gets the value of the rnabn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRnabn() {
        return rnabn;
    }

    /**
     * Sets the value of the rnabn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRnabn(String value) {
        this.rnabn = value;
    }

    /**
     * Gets the value of the nz property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNz() {
        return nz;
    }

    /**
     * Sets the value of the nz property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNz(Integer value) {
        this.nz = value;
    }

}
