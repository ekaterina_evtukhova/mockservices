
package ru.argustelecom.jbossws.liragate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Nz" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="Ku" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="Nd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BuildID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="Kb" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Abon" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Tl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="Kl" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="BmTypeID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="Nz1" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="SrcID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="Bn" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="Number" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="UstTel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "nz",
    "ku",
    "nd",
    "buildID",
    "kb",
    "abon",
    "name",
    "tl",
    "type",
    "kl",
    "bmTypeID",
    "nz1",
    "srcID",
    "bn",
    "number",
    "ustTel"
})
@XmlRootElement(name = "GetAvt")
public class GetAvt {

    @XmlElement(name = "Nz")
    protected Integer nz;
    @XmlElement(name = "Ku")
    protected Integer ku;
    @XmlElement(name = "Nd")
    protected String nd;
    @XmlElement(name = "BuildID")
    protected Integer buildID;
    @XmlElement(name = "Kb")
    protected String kb;
    @XmlElement(name = "Abon")
    protected String abon;
    @XmlElement(name = "Name")
    protected String name;
    @XmlElement(name = "Tl")
    protected String tl;
    @XmlElement(name = "Type")
    protected int type;
    @XmlElement(name = "Kl")
    protected Integer kl;
    @XmlElement(name = "BmTypeID")
    protected Integer bmTypeID;
    @XmlElement(name = "Nz1")
    protected Integer nz1;
    @XmlElement(name = "SrcID")
    protected Integer srcID;
    @XmlElement(name = "Bn")
    protected Integer bn;
    @XmlElement(name = "Number")
    protected Integer number;
    @XmlElement(name = "UstTel")
    protected String ustTel;

    /**
     * Gets the value of the nz property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNz() {
        return nz;
    }

    /**
     * Sets the value of the nz property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNz(Integer value) {
        this.nz = value;
    }

    /**
     * Gets the value of the ku property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getKu() {
        return ku;
    }

    /**
     * Sets the value of the ku property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setKu(Integer value) {
        this.ku = value;
    }

    /**
     * Gets the value of the nd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNd() {
        return nd;
    }

    /**
     * Sets the value of the nd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNd(String value) {
        this.nd = value;
    }

    /**
     * Gets the value of the buildID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBuildID() {
        return buildID;
    }

    /**
     * Sets the value of the buildID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBuildID(Integer value) {
        this.buildID = value;
    }

    /**
     * Gets the value of the kb property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKb() {
        return kb;
    }

    /**
     * Sets the value of the kb property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKb(String value) {
        this.kb = value;
    }

    /**
     * Gets the value of the abon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAbon() {
        return abon;
    }

    /**
     * Sets the value of the abon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAbon(String value) {
        this.abon = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the tl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTl() {
        return tl;
    }

    /**
     * Sets the value of the tl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTl(String value) {
        this.tl = value;
    }

    /**
     * Gets the value of the type property.
     * 
     */
    public int getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     */
    public void setType(int value) {
        this.type = value;
    }

    /**
     * Gets the value of the kl property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getKl() {
        return kl;
    }

    /**
     * Sets the value of the kl property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setKl(Integer value) {
        this.kl = value;
    }

    /**
     * Gets the value of the bmTypeID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBmTypeID() {
        return bmTypeID;
    }

    /**
     * Sets the value of the bmTypeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBmTypeID(Integer value) {
        this.bmTypeID = value;
    }

    /**
     * Gets the value of the nz1 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNz1() {
        return nz1;
    }

    /**
     * Sets the value of the nz1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNz1(Integer value) {
        this.nz1 = value;
    }

    /**
     * Gets the value of the srcID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSrcID() {
        return srcID;
    }

    /**
     * Sets the value of the srcID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSrcID(Integer value) {
        this.srcID = value;
    }

    /**
     * Gets the value of the bn property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBn() {
        return bn;
    }

    /**
     * Sets the value of the bn property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBn(Integer value) {
        this.bn = value;
    }

    /**
     * Gets the value of the number property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumber() {
        return number;
    }

    /**
     * Sets the value of the number property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumber(Integer value) {
        this.number = value;
    }

    /**
     * Gets the value of the ustTel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUstTel() {
        return ustTel;
    }

    /**
     * Sets the value of the ustTel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUstTel(String value) {
        this.ustTel = value;
    }

}
