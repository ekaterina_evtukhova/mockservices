
package ru.argustelecom.jbossws.liragate;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Row" type="{http://www.argustelecom.ru/jbossws/LIRAGate}NaryadRowType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Test" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "row",
    "test"
})
@XmlRootElement(name = "Naryad")
public class Naryad {

    @XmlElement(name = "Row")
    protected List<NaryadRowType> row;
    @XmlElement(name = "Test")
    protected Integer test;

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NaryadRowType }
     * 
     * 
     */
    public List<NaryadRowType> getRow() {
        if (row == null) {
            row = new ArrayList<NaryadRowType>();
        }
        return this.row;
    }

    public void addRow(NaryadRowType naryadRowType) {
        if (row == null) {
            row = new ArrayList<NaryadRowType>();
        }
        row.add(naryadRowType);
    }

    /**
     * Gets the value of the test property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTest() {
        return test;
    }

    /**
     * Sets the value of the test property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTest(Integer value) {
        this.test = value;
    }

}
