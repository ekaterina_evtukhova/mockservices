
package ru.argustelecom.jbossws.liragate;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.argustelecom.jbossws.liragate package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.argustelecom.jbossws.liragate
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ClearBron }
     * 
     */
    public ClearBron createClearBron() {
        return new ClearBron();
    }

    /**
     * Create an instance of {@link ClearBronResponse }
     * 
     */
    public ClearBronResponse createClearBronResponse() {
        return new ClearBronResponse();
    }

    /**
     * Create an instance of {@link GetAvt }
     * 
     */
    public GetAvt createGetAvt() {
        return new GetAvt();
    }

    /**
     * Create an instance of {@link GetAvtResponse }
     * 
     */
    public GetAvtResponse createGetAvtResponse() {
        return new GetAvtResponse();
    }

    /**
     * Create an instance of {@link DopTelRowType }
     * 
     */
    public DopTelRowType createDopTelRowType() {
        return new DopTelRowType();
    }

    /**
     * Create an instance of {@link GetLine }
     * 
     */
    public GetLine createGetLine() {
        return new GetLine();
    }

    /**
     * Create an instance of {@link GetLineResponse }
     * 
     */
    public GetLineResponse createGetLineResponse() {
        return new GetLineResponse();
    }

    /**
     * Create an instance of {@link GetNaryadLog }
     * 
     */
    public GetNaryadLog createGetNaryadLog() {
        return new GetNaryadLog();
    }

    /**
     * Create an instance of {@link GetNaryadLogResponse }
     * 
     */
    public GetNaryadLogResponse createGetNaryadLogResponse() {
        return new GetNaryadLogResponse();
    }

    /**
     * Create an instance of {@link GetNaryadLogRowType }
     * 
     */
    public GetNaryadLogRowType createGetNaryadLogRowType() {
        return new GetNaryadLogRowType();
    }

    /**
     * Create an instance of {@link GetTel }
     * 
     */
    public GetTel createGetTel() {
        return new GetTel();
    }

    /**
     * Create an instance of {@link GetTelResponse }
     * 
     */
    public GetTelResponse createGetTelResponse() {
        return new GetTelResponse();
    }

    /**
     * Create an instance of {@link GetTelRowType }
     * 
     */
    public GetTelRowType createGetTelRowType() {
        return new GetTelRowType();
    }

    /**
     * Create an instance of {@link GetTraceLog }
     * 
     */
    public GetTraceLog createGetTraceLog() {
        return new GetTraceLog();
    }

    /**
     * Create an instance of {@link GetTraceLogResponse }
     * 
     */
    public GetTraceLogResponse createGetTraceLogResponse() {
        return new GetTraceLogResponse();
    }

    /**
     * Create an instance of {@link Naryad }
     * 
     */
    public Naryad createNaryad() {
        return new Naryad();
    }

    /**
     * Create an instance of {@link NaryadRowType }
     * 
     */
    public NaryadRowType createNaryadRowType() {
        return new NaryadRowType();
    }

    /**
     * Create an instance of {@link NaryadResponse }
     * 
     */
    public NaryadResponse createNaryadResponse() {
        return new NaryadResponse();
    }

    /**
     * Create an instance of {@link SelectBron }
     * 
     */
    public SelectBron createSelectBron() {
        return new SelectBron();
    }

    /**
     * Create an instance of {@link SelectBronResponse }
     * 
     */
    public SelectBronResponse createSelectBronResponse() {
        return new SelectBronResponse();
    }

}
