
package ru.argustelecom.jbossws.liragate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for NaryadRowType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NaryadRowType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Rnabn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Clnt_id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Nz" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="MassSp" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="Cou" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="Usl" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="Tl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Abon" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Ndog" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Ku" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="Nd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Kw" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Korp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Nz1" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="Kkt" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="Priz" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DataBron" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="Tl1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ET" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="POD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="KR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DateExec" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="AbonAddr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DateDeliv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NaryadRowType", propOrder = {
    "rnabn",
    "clntId",
    "nz",
    "massSp",
    "cou",
    "usl",
    "tl",
    "abon",
    "name",
    "ndog",
    "ku",
    "nd",
    "kw",
    "korp",
    "nz1",
    "kkt",
    "priz",
    "dataBron",
    "tl1",
    "et",
    "pod",
    "kr",
    "dateExec",
    "abonAddr",
    "dateDeliv"
})
public class NaryadRowType {

    @XmlElement(name = "Rnabn")
    protected String rnabn;
    @XmlElement(name = "Clnt_id")
    protected String clntId;
    @XmlElement(name = "Nz")
    protected Integer nz;
    @XmlElement(name = "MassSp")
    protected Integer massSp;
    @XmlElement(name = "Cou")
    protected int cou;
    @XmlElement(name = "Usl")
    protected Integer usl;
    @XmlElement(name = "Tl")
    protected String tl;
    @XmlElement(name = "Abon")
    protected String abon;
    @XmlElement(name = "Name")
    protected String name;
    @XmlElement(name = "Ndog")
    protected String ndog;
    @XmlElement(name = "Ku")
    protected Integer ku;
    @XmlElement(name = "Nd")
    protected String nd;
    @XmlElement(name = "Kw")
    protected String kw;
    @XmlElement(name = "Korp")
    protected String korp;
    @XmlElement(name = "Nz1")
    protected Integer nz1;
    @XmlElement(name = "Kkt")
    protected Integer kkt;
    @XmlElement(name = "Priz")
    protected String priz;
    @XmlElement(name = "DataBron")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataBron;
    @XmlElement(name = "Tl1")
    protected String tl1;
    @XmlElement(name = "ET")
    protected Integer et;
    @XmlElement(name = "POD")
    protected String pod;
    @XmlElement(name = "KR")
    protected String kr;
    @XmlElement(name = "DateExec")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateExec;
    @XmlElement(name = "AbonAddr")
    protected String abonAddr;
    @XmlElement(name = "DateDeliv")
    protected String dateDeliv;

    /**
     * Gets the value of the rnabn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRnabn() {
        return rnabn;
    }

    /**
     * Sets the value of the rnabn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRnabn(String value) {
        this.rnabn = value;
    }

    /**
     * Gets the value of the clntId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClntId() {
        return clntId;
    }

    /**
     * Sets the value of the clntId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClntId(String value) {
        this.clntId = value;
    }

    /**
     * Gets the value of the nz property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNz() {
        return nz;
    }

    /**
     * Sets the value of the nz property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNz(Integer value) {
        this.nz = value;
    }

    /**
     * Gets the value of the massSp property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMassSp() {
        return massSp;
    }

    /**
     * Sets the value of the massSp property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMassSp(Integer value) {
        this.massSp = value;
    }

    /**
     * Gets the value of the cou property.
     * 
     */
    public int getCou() {
        return cou;
    }

    /**
     * Sets the value of the cou property.
     * 
     */
    public void setCou(int value) {
        this.cou = value;
    }

    /**
     * Gets the value of the usl property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getUsl() {
        return usl;
    }

    /**
     * Sets the value of the usl property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setUsl(Integer value) {
        this.usl = value;
    }

    /**
     * Gets the value of the tl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTl() {
        return tl;
    }

    /**
     * Sets the value of the tl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTl(String value) {
        this.tl = value;
    }

    /**
     * Gets the value of the abon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAbon() {
        return abon;
    }

    /**
     * Sets the value of the abon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAbon(String value) {
        this.abon = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the ndog property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNdog() {
        return ndog;
    }

    /**
     * Sets the value of the ndog property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNdog(String value) {
        this.ndog = value;
    }

    /**
     * Gets the value of the ku property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getKu() {
        return ku;
    }

    /**
     * Sets the value of the ku property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setKu(Integer value) {
        this.ku = value;
    }

    /**
     * Gets the value of the nd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNd() {
        return nd;
    }

    /**
     * Sets the value of the nd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNd(String value) {
        this.nd = value;
    }

    /**
     * Gets the value of the kw property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKw() {
        return kw;
    }

    /**
     * Sets the value of the kw property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKw(String value) {
        this.kw = value;
    }

    /**
     * Gets the value of the korp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKorp() {
        return korp;
    }

    /**
     * Sets the value of the korp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKorp(String value) {
        this.korp = value;
    }

    /**
     * Gets the value of the nz1 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNz1() {
        return nz1;
    }

    /**
     * Sets the value of the nz1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNz1(Integer value) {
        this.nz1 = value;
    }

    /**
     * Gets the value of the kkt property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getKkt() {
        return kkt;
    }

    /**
     * Sets the value of the kkt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setKkt(Integer value) {
        this.kkt = value;
    }

    /**
     * Gets the value of the priz property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriz() {
        return priz;
    }

    /**
     * Sets the value of the priz property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriz(String value) {
        this.priz = value;
    }

    /**
     * Gets the value of the dataBron property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataBron() {
        return dataBron;
    }

    /**
     * Sets the value of the dataBron property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataBron(XMLGregorianCalendar value) {
        this.dataBron = value;
    }

    /**
     * Gets the value of the tl1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTl1() {
        return tl1;
    }

    /**
     * Sets the value of the tl1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTl1(String value) {
        this.tl1 = value;
    }

    /**
     * Gets the value of the et property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getET() {
        return et;
    }

    /**
     * Sets the value of the et property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setET(Integer value) {
        this.et = value;
    }

    /**
     * Gets the value of the pod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOD() {
        return pod;
    }

    /**
     * Sets the value of the pod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOD(String value) {
        this.pod = value;
    }

    /**
     * Gets the value of the kr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKR() {
        return kr;
    }

    /**
     * Sets the value of the kr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKR(String value) {
        this.kr = value;
    }

    /**
     * Gets the value of the dateExec property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateExec() {
        return dateExec;
    }

    /**
     * Sets the value of the dateExec property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateExec(XMLGregorianCalendar value) {
        this.dateExec = value;
    }

    /**
     * Gets the value of the abonAddr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAbonAddr() {
        return abonAddr;
    }

    /**
     * Sets the value of the abonAddr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAbonAddr(String value) {
        this.abonAddr = value;
    }

    /**
     * Gets the value of the dateDeliv property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateDeliv() {
        return dateDeliv;
    }

    /**
     * Sets the value of the dateDeliv property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateDeliv(String value) {
        this.dateDeliv = value;
    }

}
