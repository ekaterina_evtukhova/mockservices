
package ru.argustelecom.jbossws.liragate;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Nz" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "nz"
})
@XmlRootElement(name = "ClearBron")
public class ClearBron {

    @XmlElement(name = "Nz")
    protected int nz;

    /**
     * Gets the value of the nz property.
     * 
     */
    public int getNz() {
        return nz;
    }

    /**
     * Sets the value of the nz property.
     * 
     */
    public void setNz(int value) {
        this.nz = value;
    }

}
