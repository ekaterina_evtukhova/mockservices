
package ru.argustelecom.jbossws.liragate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Err" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Stline" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="St" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Rk" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Tl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Kkt" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="Rnabn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Tl1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Td" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Length" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/&gt;
 *         &lt;element name="AdslPortType" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="AdslAnnexType" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="Rs" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Td82" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "err",
    "stline",
    "st",
    "rk",
    "tl",
    "kkt",
    "rnabn",
    "tl1",
    "td",
    "length",
    "adslPortType",
    "adslAnnexType",
    "rs",
    "td82"
})
@XmlRootElement(name = "GetLineResponse")
public class GetLineResponse {

    @XmlElement(name = "Err", required = true)
    protected String err;
    @XmlElement(name = "Stline", required = true)
    protected String stline;
    @XmlElement(name = "St", required = true)
    protected String st;
    @XmlElement(name = "Rk")
    protected String rk;
    @XmlElement(name = "Tl")
    protected String tl;
    @XmlElement(name = "Kkt")
    protected Integer kkt;
    @XmlElement(name = "Rnabn")
    protected String rnabn;
    @XmlElement(name = "Tl1")
    protected String tl1;
    @XmlElement(name = "Td")
    protected String td;
    @XmlElement(name = "Length")
    protected Double length;
    @XmlElement(name = "AdslPortType")
    protected Integer adslPortType;
    @XmlElement(name = "AdslAnnexType")
    protected Integer adslAnnexType;
    @XmlElement(name = "Rs", required = true)
    protected String rs;
    @XmlElement(name = "Td82", required = true)
    protected String td82;

    /**
     * Gets the value of the err property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErr() {
        return err;
    }

    /**
     * Sets the value of the err property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErr(String value) {
        this.err = value;
    }

    /**
     * Gets the value of the stline property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStline() {
        return stline;
    }

    /**
     * Sets the value of the stline property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStline(String value) {
        this.stline = value;
    }

    /**
     * Gets the value of the st property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSt() {
        return st;
    }

    /**
     * Sets the value of the st property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSt(String value) {
        this.st = value;
    }

    /**
     * Gets the value of the rk property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRk() {
        return rk;
    }

    /**
     * Sets the value of the rk property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRk(String value) {
        this.rk = value;
    }

    /**
     * Gets the value of the tl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTl() {
        return tl;
    }

    /**
     * Sets the value of the tl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTl(String value) {
        this.tl = value;
    }

    /**
     * Gets the value of the kkt property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getKkt() {
        return kkt;
    }

    /**
     * Sets the value of the kkt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setKkt(Integer value) {
        this.kkt = value;
    }

    /**
     * Gets the value of the rnabn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRnabn() {
        return rnabn;
    }

    /**
     * Sets the value of the rnabn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRnabn(String value) {
        this.rnabn = value;
    }

    /**
     * Gets the value of the tl1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTl1() {
        return tl1;
    }

    /**
     * Sets the value of the tl1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTl1(String value) {
        this.tl1 = value;
    }

    /**
     * Gets the value of the td property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTd() {
        return td;
    }

    /**
     * Sets the value of the td property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTd(String value) {
        this.td = value;
    }

    /**
     * Gets the value of the length property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getLength() {
        return length;
    }

    /**
     * Sets the value of the length property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setLength(Double value) {
        this.length = value;
    }

    /**
     * Gets the value of the adslPortType property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAdslPortType() {
        return adslPortType;
    }

    /**
     * Sets the value of the adslPortType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAdslPortType(Integer value) {
        this.adslPortType = value;
    }

    /**
     * Gets the value of the adslAnnexType property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAdslAnnexType() {
        return adslAnnexType;
    }

    /**
     * Sets the value of the adslAnnexType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAdslAnnexType(Integer value) {
        this.adslAnnexType = value;
    }

    /**
     * Gets the value of the rs property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRs() {
        return rs;
    }

    /**
     * Sets the value of the rs property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRs(String value) {
        this.rs = value;
    }

    /**
     * Gets the value of the td82 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTd82() {
        return td82;
    }

    /**
     * Sets the value of the td82 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTd82(String value) {
        this.td82 = value;
    }

}
