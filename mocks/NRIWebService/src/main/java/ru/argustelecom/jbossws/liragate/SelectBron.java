
package ru.argustelecom.jbossws.liragate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Nz" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="Nz1" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "nz",
    "nz1"
})
@XmlRootElement(name = "SelectBron")
public class SelectBron {

    @XmlElement(name = "Nz")
    protected int nz;
    @XmlElement(name = "Nz1")
    protected int nz1;

    /**
     * Gets the value of the nz property.
     * 
     */
    public int getNz() {
        return nz;
    }

    /**
     * Sets the value of the nz property.
     * 
     */
    public void setNz(int value) {
        this.nz = value;
    }

    /**
     * Gets the value of the nz1 property.
     * 
     */
    public int getNz1() {
        return nz1;
    }

    /**
     * Sets the value of the nz1 property.
     * 
     */
    public void setNz1(int value) {
        this.nz1 = value;
    }

}
