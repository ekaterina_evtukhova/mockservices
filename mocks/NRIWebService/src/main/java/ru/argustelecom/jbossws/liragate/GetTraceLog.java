
package ru.argustelecom.jbossws.liragate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TraceRunNumber" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "traceRunNumber"
})
@XmlRootElement(name = "GetTraceLog")
public class GetTraceLog {

    @XmlElement(name = "TraceRunNumber")
    protected int traceRunNumber;

    /**
     * Gets the value of the traceRunNumber property.
     * 
     */
    public int getTraceRunNumber() {
        return traceRunNumber;
    }

    /**
     * Sets the value of the traceRunNumber property.
     * 
     */
    public void setTraceRunNumber(int value) {
        this.traceRunNumber = value;
    }

}
