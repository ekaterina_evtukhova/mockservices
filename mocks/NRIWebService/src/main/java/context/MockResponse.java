package context;

import com.fasterxml.jackson.databind.node.ObjectNode;
import ru.deltasolutions.oms.smart.mocker.mockinterface.requests.SetResponseRequest;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Katy on 08.04.2016.
 */
public class MockResponse {
    private LinkedHashMap<String, List<ObjectNode>> statSyncResponses;

    public Integer getStatSyncResponsesCountByMethod(String method) {
        return getStatSyncResponseByMethod(method).size();
    }

    public LinkedHashMap<String, List<ObjectNode>> getStatSyncResponses() {
        return statSyncResponses;
    }

    public List<ObjectNode> getStatSyncResponseByMethod(String method) {
        return statSyncResponses.get(method);
    }

    public ObjectNode getStatSyncResponse(String method, Integer i) {
        return getStatSyncResponseByMethod(method).get(i);
    }

    public void setStatSyncResponses(SetResponseRequest setResponseRequest) {
        statSyncResponses = new LinkedHashMap<>();
        for (String method : setResponseRequest.getRequests().keySet()) {

            List<ObjectNode> objectNodes = setResponseRequest.getRequests().get(method);
            statSyncResponses.put(method, objectNodes);
        }
    }


}
