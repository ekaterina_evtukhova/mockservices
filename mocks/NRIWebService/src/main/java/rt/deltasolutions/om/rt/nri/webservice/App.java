package rt.deltasolutions.om.rt.nri.webservice;

import javax.ws.rs.core.Application;
import javax.ws.rs.ApplicationPath;

/**
 * Created by Katy on 06.04.2016.
 */
@ApplicationPath("/")
public class App  extends Application {
}
