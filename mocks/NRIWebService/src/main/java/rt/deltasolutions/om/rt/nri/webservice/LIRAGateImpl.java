package rt.deltasolutions.om.rt.nri.webservice;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import context.MockResponse;
import ru.argustelecom.jbossws.liragate.*;
import ru.deltasolutions.oms.smart.mocker.mockinterface.requests.SetResponseRequest;
import ru.deltasolutions.oms.smart.mocker.mockinterface.responses.AssertInfo;

import javax.jws.WebService;
import javax.ws.rs.Path;
import javax.xml.ws.Holder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Katy on 11.03.2016.
 */
@WebService(endpointInterface = "ru.argustelecom.jbossws.liragate.LIRAGate")
@Path("/LIRAGateImpl")
public class LIRAGateImpl implements LIRAGate {
    private final ObjectMapper objectMapper = new ObjectMapper();
    private static HashMap<String, ArrayList<ObjectNode>> statRequests = new HashMap<>();
    private static HashMap<String, ArrayList<ObjectNode>> statResponses = new HashMap<>();

    private static MockResponse syncResponse = new MockResponse();

    private static Integer responseNumClearBron = 0;
    private static Integer responseNumNaryad = 0;

    @Deprecated
    public void getAvt(Integer nz, Integer ku, String nd, Integer buildID, String kb, String abon,
                       String name, Holder<String> tl, int type, Integer kl, Integer bmTypeID, Integer nz1,
                       Integer srcID, Integer bn, Integer number, String ustTel, Holder<String> err,
                       Holder<String> strline, Holder<String> st, Holder<Integer> kkt, Holder<String> td,
                       Holder<Double> length, Holder<Integer> adslPortType, Holder<Integer> adslAnnexType,
                       Holder<String> rs, Holder<String> td82, Holder<List<DopTelRowType>> dopTel) {

    }

    public String clearBron(int nz) {
        String method = "clearBron";

        String err = syncResponse.getStatSyncResponse(method, responseNumClearBron).get("err").asText();

        //add response
        ObjectNode node = objectMapper.createObjectNode();
        node.put("err", err);
        addToStatisticMap(node, statResponses, method);

        //add request
        node = objectMapper.createObjectNode();
        node.put("nz", nz);
        addToStatisticMap(node, statRequests, method);

        responseNumClearBron = (responseNumClearBron + 1) % (syncResponse.getStatSyncResponsesCountByMethod(method));

        return err;
    }

    @Deprecated
    public void getLine(Holder<String> tl, Holder<Integer> kkt, Holder<String> rnabn, Integer nz,
                        Holder<String> err, Holder<String> stline, Holder<String> st, Holder<String> rk,
                        Holder<String> tl1, Holder<String> td, Holder<Double> length, Holder<Integer> adslPortType,
                        Holder<Integer> adslAnnexType, Holder<String> rs, Holder<String> td82) {

    }

    @Deprecated
    public String selectBron(int nz, int nz1) {
        return null;
    }

    @Deprecated
    public void getTraceLog(int traceRunNumber, Holder<String> log, Holder<String> err) {

    }

    public String naryad(List<NaryadRowType> row, Integer test) {
        String method = "naryad";

        String err = syncResponse.getStatSyncResponse(method, responseNumNaryad).get("err").asText();

        //add response
        ObjectNode node = objectMapper.createObjectNode();
        node.put("err", err);
        addToStatisticMap(node, statResponses, method);

        //add request
        node = objectMapper.createObjectNode();
        node.put("test", test);
        JsonNode jsonNode = objectMapper.valueToTree(row);
        for (JsonNode tmp : ((ArrayNode)jsonNode)) {
            ((ObjectNode)tmp).remove("dateDeliv");
            ((ObjectNode)tmp).remove("dateExec");
        }
        node.set("row", jsonNode);

        addToStatisticMap(node, statRequests, method);

        responseNumNaryad = (responseNumNaryad + 1) % (syncResponse.getStatSyncResponsesCountByMethod(method));

        return err;
    }

    @Deprecated
    public void getTel(String nomAts, Integer type, Integer number, Holder<String> err,
                       Holder<List<GetTelRowType>> row) {

    }

    @Deprecated
    public void getNaryadLog(String tl, Integer kkt, String rnabn, Integer nz, Integer nzGroup,
                             Holder<String> err, Holder<List<GetNaryadLogRowType>> row) {

    }

    // WARN! Side effect
    private void addToStatisticMap(ObjectNode statEntry, HashMap<String, ArrayList<ObjectNode>> statMap, String methodName) {
        ArrayList<ObjectNode> objectNodes = statMap.get(methodName);
        if (objectNodes == null) {
            objectNodes = new ArrayList<>();
        }
        objectNodes.add(statEntry);
        statMap.put(methodName, objectNodes);
    }

    @Override
    public void setResponse(SetResponseRequest setResponseRequest) {
        responseNumClearBron = 0;
        responseNumNaryad = 0;
        syncResponse = new MockResponse();
        statResponses = new HashMap<>();
        statRequests = new HashMap<>();
        syncResponse.setStatSyncResponses(setResponseRequest);
    }

    @Override
    public void setAsync(SetResponseRequest setResponseRequest) {

    }

    @Override
    public AssertInfo getAssertInfo() {
        AssertInfo assertInfo = new AssertInfo();
        assertInfo.setResponses(new HashMap<>(statResponses));
        assertInfo.setRequests(new HashMap<>(statRequests));
        statResponses.clear();
        statRequests.clear();
        return assertInfo;
    }

}
