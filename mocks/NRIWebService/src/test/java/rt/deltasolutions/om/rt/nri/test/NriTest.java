package rt.deltasolutions.om.rt.nri.test;

import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;
import org.junit.runner.RunWith;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;
import ru.argustelecom.jbossws.liragate.*;
import ru.deltasolutions.oms.smart.mocker.mocker.annotation.BloodyTest;
import ru.deltasolutions.oms.smart.mocker.mocker.runner.BloodyMockerTestRunner;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Katy on 06.04.2016.
 */
@RunWith(BloodyMockerTestRunner.class)
public class NriTest {

    @BloodyTest(pathToConfig = "nriTestFile.json")
    public void nriTest2() {}

    @BloodyTest(pathToConfig = "nriTestFile.json")
    public void nriTest() {
        WebServiceTemplate webServiceTemplate = new WebServiceTemplate();

        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        try {
            marshaller.setContextPath("ru.argustelecom.jbossws.liragate");
            marshaller.afterPropertiesSet();

        } catch (Exception e) {
            e.printStackTrace();
        }

        Jaxb2Marshaller unmarshaller = new Jaxb2Marshaller();
        try {
            unmarshaller.setContextPath("ru.argustelecom.jbossws.liragate");
            unmarshaller.afterPropertiesSet();
        } catch (Exception e) {
            e.printStackTrace();
        }

        webServiceTemplate.setMarshaller(marshaller);
        webServiceTemplate.setUnmarshaller(unmarshaller);

        Integer test = 0;
        Naryad naryad = new Naryad();
        naryad.setTest(test);
        NaryadRowType row = new NaryadRowType();
        row.setRnabn("11081141");
        row.setNz(43701360);
        row.setCou(1);
        row.setUsl(50001);
        row.setTl("3432974112");
        row.setAbon("qwerty");
        row.setNdog("2215008");
        row.setKu(203058);
        row.setNd("20");
        row.setKw("129");
        row.setKkt(1);

        Long time = 1375300800000L;
        Date date = new Date(time);
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        XMLGregorianCalendar xmlDate2 = null;
        try {
        xmlDate2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
        row.setDateExec(xmlDate2);

        row.setET(0);
        row.setPOD("");
        row.setKR("");

        row.setDateDeliv("01.08.2013/09:00-12:00");

        naryad.addRow(row);
        NaryadResponse resultNaryad = (NaryadResponse) webServiceTemplate.marshalSendAndReceive("http://localhost:8080/NRIWebService-1.0.0-SNAPSHOT/LIRAGateImpl",
                naryad);
        System.out.println(resultNaryad);


        Integer nz = 70703444;

        ClearBron clearBron = new ClearBron();
        clearBron.setNz(nz);
        ClearBronResponse result = (ClearBronResponse) webServiceTemplate.marshalSendAndReceive("http://localhost:8080/NRIWebService-1.0.0-SNAPSHOT/LIRAGateImpl",
                clearBron);
        System.out.println(result);

    }
}
