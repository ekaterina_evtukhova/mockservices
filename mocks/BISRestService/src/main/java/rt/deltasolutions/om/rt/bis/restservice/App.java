package rt.deltasolutions.om.rt.bis.restservice;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Created by Katy on 16.03.2016.
 */
@ApplicationPath("/")
public class App  extends Application{
}
