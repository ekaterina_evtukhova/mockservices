package rt.deltasolutions.om.rt.bis.restservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import context.BisResponse;
import context.SyncResponse;
import ru.deltasolutions.oms.smart.mocker.mockinterface.MockInterface;
import ru.deltasolutions.oms.smart.mocker.mockinterface.requests.SetResponseRequest;
import ru.deltasolutions.oms.smart.mocker.mockinterface.responses.AssertInfo;
import xmlparser.XMLParser;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Katy on 15.03.2016.
 */
@Path("/LIRA/LIRA_INT")
public class BisRestServicePublisher implements MockInterface {
    private final ObjectMapper objectMapper = new ObjectMapper();
    private static HashMap<String, ArrayList<ObjectNode>> statRequests = new HashMap<>();
    private static HashMap<String, ArrayList<ObjectNode>> statResponses = new HashMap<>();

    private static SyncResponse syncResponse;

    private static Integer responseNum = 0;

    @GET
    @Produces("text/plain")
    public Response addServiceData(@QueryParam("PACTION") String action) {
        XMLParser parser = new XMLParser();
        String method = "addServiceData";

        String errText = syncResponse.getStatSyncResponse(method, responseNum).get("errText").asText();
        String errCode = syncResponse.getStatSyncResponse(method, responseNum).get("errCode").asText();
        String subsId = syncResponse.getStatSyncResponse(method, responseNum).get("subsId").asText();

        //add response
        String str = parser.getResponse(action, errText, errCode, subsId);
        BisResponse bisResponse = new BisResponse();
        bisResponse.setValue(str);
        addToStatisticMap(objectMapper.valueToTree(bisResponse), statResponses, method);

        //add request
        ObjectNode node = objectMapper.createObjectNode();
        node.put("value", action);
        addToStatisticMap(node, statRequests, method);

        responseNum = (responseNum + 1) % (syncResponse.getStatSyncResponsesCountByMethod(method));

        Response response = Response.ok(str).type("text/plain").build();
        return response;
    }

    // WARN! Side effect
    private void addToStatisticMap(ObjectNode statEntry, HashMap<String, ArrayList<ObjectNode>> statMap, String methodName) {
        ArrayList<ObjectNode> objectNodes = statMap.get(methodName);
        if (objectNodes == null) {
            objectNodes = new ArrayList<>();
        }
        objectNodes.add(objectMapper.valueToTree(statEntry));
        statMap.put(methodName, objectNodes);
    }

    @Override
    public void setResponse(SetResponseRequest setResponseRequest) {
        syncResponse = new SyncResponse();
        responseNum = 0;
        statResponses = new HashMap<>();
        statRequests = new HashMap<>();
        syncResponse.setStatSyncResponses(setResponseRequest);
    }

    @Override
    public void setAsync(SetResponseRequest setResponseRequest) {
        //no async responses from bis!
    }

    @Override
    public AssertInfo getAssertInfo() {
        AssertInfo assertInfo = new AssertInfo();
        assertInfo.setResponses(new HashMap<>(statResponses));
        assertInfo.setRequests(new HashMap<>(statRequests));
        statResponses.clear();
        statRequests.clear();
        return assertInfo;
    }
}
