package xmlparser;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.InputStream;
import java.io.StringWriter;
/**
 * Created by Katy on 15.03.2016.
 */

public class XMLParser {

    private static String toString(Document doc) {
        StringWriter sw = new StringWriter();
        try {
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

            transformer.transform(new DOMSource(doc), new StreamResult(sw));
        } catch (TransformerException e) {
            e.printStackTrace();
        }
        return sw.toString();

    }


    public String getResponse(String action, String errText, String errCode, String subsId) {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is;
        if (action.equals("1002")) {
            is = classloader.getResourceAsStream("META-INF/response1002.xml");
        } else {
            is = classloader.getResourceAsStream("META-INF/response.xml");
        }

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();

        DocumentBuilder dBuilder = null;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        Document doc = null;
        try {
            doc = dBuilder.parse(is);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (action.equals("1") || action.equals("1002")) {
            doc.getElementsByTagName("PSUBS_ID").item(0).setTextContent(subsId);
        } else {
            Element subs = (Element) doc.getElementsByTagName("PSUBS_ID").item(0);
            subs.getParentNode().removeChild(subs);
        }
        doc.getElementsByTagName("PERR_CODE").item(0).setTextContent(errCode);
        doc.getElementsByTagName("PERR_TEXT").item(0).setTextContent(errText);

        return toString(doc);
    }
}
