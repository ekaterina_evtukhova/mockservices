package context;

import lombok.Data;

/**
 * Created by Katy on 01.04.2016.
 */

public class BisResponse {
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    private String value;
}
