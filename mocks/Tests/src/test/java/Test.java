import org.junit.runner.RunWith;
import ru.deltasolutions.oms.smart.mocker.mocker.annotation.BloodyTest;
import ru.deltasolutions.oms.smart.mocker.mocker.runner.BloodyMockerTestRunner;

/**
 * Created by Katy on 27.04.2016.
 */
@RunWith(BloodyMockerTestRunner.class)
public class Test {

    @BloodyTest(pathToConfig = "test1.json")
    public void test1() throws InterruptedException {
        Thread.sleep(170000);
    }

    @BloodyTest(pathToConfig = "test2.json")
    public void test2() throws InterruptedException {
        Thread.sleep(50000);
    }

    @BloodyTest(pathToConfig = "test3.json")
    public void test3() throws InterruptedException {
        Thread.sleep(50000);
    }

    @BloodyTest(pathToConfig = "test4.json")
    public void test4() throws InterruptedException {
        Thread.sleep(50000);
    }

    @BloodyTest(pathToConfig = "test5.json")
    public void test5() throws InterruptedException {
        Thread.sleep(50000);
    }

    @BloodyTest(pathToConfig = "test6.json")
    public void test6() throws InterruptedException {
        Thread.sleep(50000);
    }

    @BloodyTest(pathToConfig = "test7.json")
    public void test7() throws InterruptedException {
        Thread.sleep(30000);
    }

    @BloodyTest(pathToConfig = "test8.json")
    public void test8() throws InterruptedException {
        Thread.sleep(30000);
    }

    @BloodyTest(pathToConfig = "test9.json")
    public void test9() throws InterruptedException {
        Thread.sleep(30000);
    }

    @BloodyTest(pathToConfig = "test10.json")
    public void test10() throws InterruptedException {
        Thread.sleep(30000);
    }

    @BloodyTest(pathToConfig = "test11.json")
    public void test11() throws InterruptedException {
        Thread.sleep(30000);
    }

    @BloodyTest(pathToConfig = "test12.json")
    public void test12() throws InterruptedException {
        Thread.sleep(30000);
    }

    @BloodyTest(pathToConfig = "test13.json")
    public void test13() throws InterruptedException {
        Thread.sleep(30000);
    }
}
