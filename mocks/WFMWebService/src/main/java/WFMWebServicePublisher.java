import ru.deltasolutions.om.rt.wfm.webservice.WfmApiForLiraAdapter;

import javax.xml.ws.Endpoint;

/**
 * Created by Katy on 11.03.2016.
 */
public class WFMWebServicePublisher {
    public static void main(String... args) {
        Endpoint.publish("http://127.0.0.1:21081/wfm", new WfmApiForLiraAdapter());
    }
}
