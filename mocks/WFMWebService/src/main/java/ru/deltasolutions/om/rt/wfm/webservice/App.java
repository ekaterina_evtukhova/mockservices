package ru.deltasolutions.om.rt.wfm.webservice;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Created by Katy on 13.04.2016.
 */
@ApplicationPath("/")
public class App  extends Application {
}
