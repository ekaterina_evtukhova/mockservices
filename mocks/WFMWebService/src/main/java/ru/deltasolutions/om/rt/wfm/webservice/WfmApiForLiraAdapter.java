package ru.deltasolutions.om.rt.wfm.webservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import context.MockResponse;
import executor.Executor;
import ru.argustelecom.liragate.wfm.*;
import ru.deltasolutions.oms.smart.mocker.mockinterface.MockInterface;
import ru.deltasolutions.oms.smart.mocker.mockinterface.requests.SetResponseRequest;
import ru.deltasolutions.oms.smart.mocker.mockinterface.responses.AssertInfo;

import javax.jws.WebService;
import javax.ws.rs.Path;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Katy on 11.03.2016.
 */
@WebService(targetNamespace = "http://wfm.liragate.argustelecom.ru/",
        endpointInterface = "ru.argustelecom.liragate.wfm.WfmApiForLiraAdapter",
        name = "WfmApiForLiraAdapter")
@Path("/WfmApiForLiraAdapter")
public class WfmApiForLiraAdapter implements ru.argustelecom.liragate.wfm.WfmApiForLiraAdapter, MockInterface {
    private final ObjectMapper objectMapper = new ObjectMapper();

    private static HashMap<String, ArrayList<ObjectNode>> statRequests = new HashMap<>();
    private static HashMap<String, ArrayList<ObjectNode>> statResponses = new HashMap<>();

    private static Integer responseNumUpdateVisit = 0;
    private static Integer responseNumUpdateVisitStatus = 0;
    private static Integer responseNumConfirmVisit = 0;
    private static Integer responseNumFinishVisit = 0;

    private static MockResponse syncResponse = new MockResponse();
    private static MockResponse asyncResponse = new MockResponse();

    public SimpleResponce confirmVisit(String liraOrderId, String status) {
        String method = "confirmVisit";

        SimpleResponce simpleResponce = generateResponses(method, responseNumConfirmVisit);

        //add request
        ObjectNode node = objectMapper.createObjectNode();
        node.put("liraOrderId", liraOrderId);
        node.put("status", status);
        addToStatisticMap(node, statRequests, method);

        String liraOrderIdFinishVisit = asyncResponse.getStatSyncResponse("finishVisit",
                responseNumFinishVisit).get("liraOrderId").asText();

        ServiceInstallationResults list = null;
        try {
            list = objectMapper.treeToValue(asyncResponse.getStatSyncResponse("finishVisit",
                    responseNumFinishVisit).get("servicesInstallationResult"), ServiceInstallationResults.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        Executor.schedule(liraOrderIdFinishVisit, asyncResponse.getStatSyncResponse("finishVisit",
                responseNumFinishVisit).get("time").asText(), list);

        responseNumConfirmVisit = (responseNumConfirmVisit + 1) %
                (syncResponse.getStatSyncResponsesCountByMethod(method));

        responseNumFinishVisit = (responseNumFinishVisit + 1) %
                (syncResponse.getStatSyncResponsesCountByMethod(method));

        return simpleResponce;
    }

    public SimpleResponce updateVisitStatus(String liraOrderId, int decision, XMLGregorianCalendar callbackDate) {
        String method = "updateVisitStatus";

        SimpleResponce simpleResponce = generateResponses(method, responseNumUpdateVisitStatus);

        //add request
        ObjectNode node = objectMapper.createObjectNode();
        node.put("liraOrderId", liraOrderId);
        node.put("decision", decision);
        node.put("callbackDate", callbackDate.toString());
        addToStatisticMap(node, statRequests, method);

        responseNumUpdateVisitStatus = (responseNumUpdateVisitStatus + 1) %
                (syncResponse.getStatSyncResponsesCountByMethod(method));

        return simpleResponce;
    }

    public SimpleResponce updateVisit(String liraOrderId, LiraOrders liraOrderList, String originator) {
        String method = "updateVisit";

        SimpleResponce simpleResponce = generateResponses(method, responseNumUpdateVisit);

        //add request
        ObjectNode node = objectMapper.createObjectNode();
        node.put("liraOrderId", liraOrderId);
        node.put("originator", originator);
        JsonNode jsonNode = objectMapper.valueToTree(liraOrderList);
        node.set("liraOrderList", jsonNode);
        addToStatisticMap(node, statRequests, method);

        responseNumUpdateVisit = (responseNumUpdateVisit + 1) %
                (syncResponse.getStatSyncResponsesCountByMethod(method));

        return simpleResponce;
    }

    private SimpleResponce generateResponses(String method, Integer num) {
        SimpleResponce simpleResponce = new SimpleResponce();
        String errorCode = syncResponse.getStatSyncResponse(method, num).get("errorCode").asText();
        String errorMsg = syncResponse.getStatSyncResponse(method, num).get("errorMsg").asText();

        simpleResponce.setErrorCode(Long.valueOf(errorCode));
        simpleResponce.setErrorMsg(errorMsg);

        ObjectNode node = objectMapper.createObjectNode();
        node.put("errorCode", errorCode);
        node.put("errorMsg", errorMsg);
        addToStatisticMap(node, statResponses, method);

        return simpleResponce;
    }

    // WARN! Side effect
    private void addToStatisticMap(ObjectNode statEntry, HashMap<String, ArrayList<ObjectNode>> statMap, String methodName) {
        ArrayList<ObjectNode> objectNodes = statMap.get(methodName);
        if (objectNodes == null) {
            objectNodes = new ArrayList<>();
        }
        objectNodes.add(statEntry);
        statMap.put(methodName, objectNodes);
    }

    @Override
    public void setResponse(SetResponseRequest setResponseRequest) {
        responseNumUpdateVisit = 0;
        responseNumUpdateVisitStatus = 0;
        responseNumConfirmVisit = 0;
        statResponses = new HashMap<>();
        statRequests = new HashMap<>();
        syncResponse = new MockResponse();
        syncResponse.setStatResponses(setResponseRequest);
    }

    @Override
    public void setAsync(SetResponseRequest setResponseRequest) {
        responseNumFinishVisit = 0;
        asyncResponse = new MockResponse();
        asyncResponse.setStatResponses(setResponseRequest);
    }

    @Override
    public AssertInfo getAssertInfo() {
        AssertInfo assertInfo = new AssertInfo();
        assertInfo.setResponses(new HashMap<>(statResponses));
        assertInfo.setRequests(new HashMap<>(statRequests));
        statResponses.clear();
        statRequests.clear();
        return assertInfo;
    }
}
