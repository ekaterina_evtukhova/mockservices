package ru.deltasolutions.om.rt.wfm.webservice;

import ru.argustelecom.liragate.wfm.*;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.GregorianCalendar;

/**
 * Created by Katy on 23.03.2016.
 */
public class WfmCallbackImpl implements Runnable{
    private String liraOrderId;
    private ServiceInstallationResults liraOrder;
    private Worker worker;
    private XMLGregorianCalendar finishDate;
    private ResourceMovementNotifications resourceMovementNotifications;

    public WfmCallbackImpl(String liraOrderId, ServiceInstallationResults liraOrder) {
        this.liraOrderId = liraOrderId;
        this.liraOrder = liraOrder;

        worker = new Worker();
        worker.setWorkerId("45");
        worker.setName("Alex");
        worker.setWorkerPhoneNumber("123456789");

        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        DatatypeFactory datatypeFactory = null;
        try {
            datatypeFactory = DatatypeFactory.newInstance();
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
        finishDate = datatypeFactory != null ?
                datatypeFactory.newXMLGregorianCalendar(gregorianCalendar) : null;

        resourceMovementNotifications = new ResourceMovementNotifications();
    }
    public void run() {
        try {
            sendAsynchResponse();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendAsynchResponse() {
        WfmToLiraApiService wfmToLiraApiService = new WfmToLiraApiService();
        LiraApiForWfm liraApiForWfm = wfmToLiraApiService.getWfmToLiraApiServiceSOAP();

        for (ServiceInstallationResult s: liraOrder.getService() ) {
            System.out.println(s.getNz() + "\n");

        }

        SimpleResponce simpleResponce = liraApiForWfm.finishVisit(liraOrderId,
                liraOrder, worker,
                worker, finishDate, resourceMovementNotifications);

        System.out.println(liraOrderId);

        System.out.println(simpleResponce.getErrorCode()  + "   " + simpleResponce.getErrorMsg());
    }
}
