
package ru.argustelecom.liragate.wfm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ServiceInstallationResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ServiceInstallationResult"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="rnabn" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="serviceTypeName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="nz" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="isInstalled" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="failReasonId" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="failReasonComment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceInstallationResult", propOrder = {
    "rnabn",
    "serviceTypeName",
    "nz",
    "isInstalled",
    "failReasonId",
    "failReasonComment"
})
public class ServiceInstallationResult {

    protected long rnabn;
    @XmlElement(required = true)
    protected String serviceTypeName;
    @XmlElement(required = true)
    protected String nz;
    protected boolean isInstalled;
    protected long failReasonId;
    protected String failReasonComment;

    /**
     * Gets the value of the rnabn property.
     * 
     */
    public long getRnabn() {
        return rnabn;
    }

    /**
     * Sets the value of the rnabn property.
     * 
     */
    public void setRnabn(long value) {
        this.rnabn = value;
    }

    /**
     * Gets the value of the serviceTypeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceTypeName() {
        return serviceTypeName;
    }

    /**
     * Sets the value of the serviceTypeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceTypeName(String value) {
        this.serviceTypeName = value;
    }

    /**
     * Gets the value of the nz property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNz() {
        return nz;
    }

    /**
     * Sets the value of the nz property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNz(String value) {
        this.nz = value;
    }

    /**
     * Gets the value of the isInstalled property.
     * 
     */
    public boolean isIsInstalled() {
        return isInstalled;
    }

    /**
     * Sets the value of the isInstalled property.
     * 
     */
    public void setIsInstalled(boolean value) {
        this.isInstalled = value;
    }

    /**
     * Gets the value of the failReasonId property.
     * 
     */
    public long getFailReasonId() {
        return failReasonId;
    }

    /**
     * Sets the value of the failReasonId property.
     * 
     */
    public void setFailReasonId(long value) {
        this.failReasonId = value;
    }

    /**
     * Gets the value of the failReasonComment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFailReasonComment() {
        return failReasonComment;
    }

    /**
     * Sets the value of the failReasonComment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFailReasonComment(String value) {
        this.failReasonComment = value;
    }

}
