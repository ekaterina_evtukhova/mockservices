
package ru.argustelecom.liragate.wfm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for assignVisitIntervalResponce complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="assignVisitIntervalResponce"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://wfm.liragate.argustelecom.ru/}simpleResponce"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="wfmOrderId" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "assignVisitIntervalResponce", propOrder = {
    "wfmOrderId"
})
public class AssignVisitIntervalResponce
    extends SimpleResponce
{

    protected long wfmOrderId;

    /**
     * Gets the value of the wfmOrderId property.
     * 
     */
    public long getWfmOrderId() {
        return wfmOrderId;
    }

    /**
     * Sets the value of the wfmOrderId property.
     * 
     */
    public void setWfmOrderId(long value) {
        this.wfmOrderId = value;
    }

}
