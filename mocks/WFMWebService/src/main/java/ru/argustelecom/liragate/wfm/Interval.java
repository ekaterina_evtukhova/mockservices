
package ru.argustelecom.liragate.wfm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for interval complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="interval"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="intervalId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="isStrictlyPlanned" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="acceptability" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="agreedstart" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="agreedfinish" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="planValue" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="actualValue" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="worksite" type="{http://wfm.liragate.argustelecom.ru/}wfmWorksite"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "interval", propOrder = {
    "intervalId",
    "isStrictlyPlanned",
    "acceptability",
    "agreedstart",
    "agreedfinish",
    "planValue",
    "actualValue",
    "worksite"
})
public class Interval {

    @XmlElement(required = true)
    protected String intervalId;
    protected boolean isStrictlyPlanned;
    protected int acceptability;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar agreedstart;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar agreedfinish;
    protected int planValue;
    protected int actualValue;
    @XmlElement(required = true)
    protected WfmWorksite worksite;

    /**
     * Gets the value of the intervalId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntervalId() {
        return intervalId;
    }

    /**
     * Sets the value of the intervalId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntervalId(String value) {
        this.intervalId = value;
    }

    /**
     * Gets the value of the isStrictlyPlanned property.
     * 
     */
    public boolean isIsStrictlyPlanned() {
        return isStrictlyPlanned;
    }

    /**
     * Sets the value of the isStrictlyPlanned property.
     * 
     */
    public void setIsStrictlyPlanned(boolean value) {
        this.isStrictlyPlanned = value;
    }

    /**
     * Gets the value of the acceptability property.
     * 
     */
    public int getAcceptability() {
        return acceptability;
    }

    /**
     * Sets the value of the acceptability property.
     * 
     */
    public void setAcceptability(int value) {
        this.acceptability = value;
    }

    /**
     * Gets the value of the agreedstart property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAgreedstart() {
        return agreedstart;
    }

    /**
     * Sets the value of the agreedstart property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAgreedstart(XMLGregorianCalendar value) {
        this.agreedstart = value;
    }

    /**
     * Gets the value of the agreedfinish property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAgreedfinish() {
        return agreedfinish;
    }

    /**
     * Sets the value of the agreedfinish property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAgreedfinish(XMLGregorianCalendar value) {
        this.agreedfinish = value;
    }

    /**
     * Gets the value of the planValue property.
     * 
     */
    public int getPlanValue() {
        return planValue;
    }

    /**
     * Sets the value of the planValue property.
     * 
     */
    public void setPlanValue(int value) {
        this.planValue = value;
    }

    /**
     * Gets the value of the actualValue property.
     * 
     */
    public int getActualValue() {
        return actualValue;
    }

    /**
     * Sets the value of the actualValue property.
     * 
     */
    public void setActualValue(int value) {
        this.actualValue = value;
    }

    /**
     * Gets the value of the worksite property.
     * 
     * @return
     *     possible object is
     *     {@link WfmWorksite }
     *     
     */
    public WfmWorksite getWorksite() {
        return worksite;
    }

    /**
     * Sets the value of the worksite property.
     * 
     * @param value
     *     allowed object is
     *     {@link WfmWorksite }
     *     
     */
    public void setWorksite(WfmWorksite value) {
        this.worksite = value;
    }

}
