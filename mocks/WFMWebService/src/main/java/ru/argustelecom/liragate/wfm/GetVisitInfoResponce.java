
package ru.argustelecom.liragate.wfm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getVisitInfoResponce complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getVisitInfoResponce"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://wfm.liragate.argustelecom.ru/}simpleResponce"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="wfmOrders"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="wfmOrder" type="{http://wfm.liragate.argustelecom.ru/}outputWfmOrder" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getVisitInfoResponce", propOrder = {
    "wfmOrders"
})
public class GetVisitInfoResponce
    extends SimpleResponce
{

    @XmlElement(required = true)
    protected GetVisitInfoResponce.WfmOrders wfmOrders;

    /**
     * Gets the value of the wfmOrders property.
     * 
     * @return
     *     possible object is
     *     {@link GetVisitInfoResponce.WfmOrders }
     *     
     */
    public GetVisitInfoResponce.WfmOrders getWfmOrders() {
        return wfmOrders;
    }

    /**
     * Sets the value of the wfmOrders property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetVisitInfoResponce.WfmOrders }
     *     
     */
    public void setWfmOrders(GetVisitInfoResponce.WfmOrders value) {
        this.wfmOrders = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="wfmOrder" type="{http://wfm.liragate.argustelecom.ru/}outputWfmOrder" maxOccurs="unbounded"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "wfmOrder"
    })
    public static class WfmOrders {

        @XmlElement(required = true)
        protected List<OutputWfmOrder> wfmOrder;

        /**
         * Gets the value of the wfmOrder property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the wfmOrder property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getWfmOrder().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link OutputWfmOrder }
         * 
         * 
         */
        public List<OutputWfmOrder> getWfmOrder() {
            if (wfmOrder == null) {
                wfmOrder = new ArrayList<OutputWfmOrder>();
            }
            return this.wfmOrder;
        }

    }

}
