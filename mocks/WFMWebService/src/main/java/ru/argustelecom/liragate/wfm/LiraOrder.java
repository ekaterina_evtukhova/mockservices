
package ru.argustelecom.liragate.wfm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for liraOrder complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="liraOrder"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="nz" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="kkt" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="tl" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="tl1" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="tl2" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="count" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "liraOrder", propOrder = {
    "nz",
    "kkt",
    "tl",
    "tl1",
    "tl2",
    "count"
})
public class LiraOrder {

    protected long nz;
    protected long kkt;
    @XmlElement(required = true)
    protected String tl;
    @XmlElement(required = true)
    protected String tl1;
    @XmlElement(required = true)
    protected String tl2;
    protected long count;

    /**
     * Gets the value of the nz property.
     * 
     */
    public long getNz() {
        return nz;
    }

    /**
     * Sets the value of the nz property.
     * 
     */
    public void setNz(long value) {
        this.nz = value;
    }

    /**
     * Gets the value of the kkt property.
     * 
     */
    public long getKkt() {
        return kkt;
    }

    /**
     * Sets the value of the kkt property.
     * 
     */
    public void setKkt(long value) {
        this.kkt = value;
    }

    /**
     * Gets the value of the tl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTl() {
        return tl;
    }

    /**
     * Sets the value of the tl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTl(String value) {
        this.tl = value;
    }

    /**
     * Gets the value of the tl1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTl1() {
        return tl1;
    }

    /**
     * Sets the value of the tl1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTl1(String value) {
        this.tl1 = value;
    }

    /**
     * Gets the value of the tl2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTl2() {
        return tl2;
    }

    /**
     * Sets the value of the tl2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTl2(String value) {
        this.tl2 = value;
    }

    /**
     * Gets the value of the count property.
     * 
     */
    public long getCount() {
        return count;
    }

    /**
     * Sets the value of the count property.
     * 
     */
    public void setCount(long value) {
        this.count = value;
    }

}
