
package ru.argustelecom.liragate.wfm;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for outputWfmOrder complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="outputWfmOrder"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="wfmOrderId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="wfmOrderNumber" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="agreedStartTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="agreedFinishTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="checkTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="registerTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="confirmed" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="worker" type="{http://wfm.liragate.argustelecom.ru/}outputWorker" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "outputWfmOrder", propOrder = {
    "wfmOrderId",
    "wfmOrderNumber",
    "agreedStartTime",
    "agreedFinishTime",
    "checkTime",
    "registerTime",
    "confirmed",
    "status",
    "worker"
})
public class OutputWfmOrder {

    @XmlElementRef(name = "wfmOrderId", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> wfmOrderId;
    @XmlElementRef(name = "wfmOrderNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> wfmOrderNumber;
    @XmlElementRef(name = "agreedStartTime", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> agreedStartTime;
    @XmlElementRef(name = "agreedFinishTime", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> agreedFinishTime;
    @XmlElementRef(name = "checkTime", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> checkTime;
    @XmlElementRef(name = "registerTime", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> registerTime;
    @XmlElementRef(name = "confirmed", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> confirmed;
    @XmlElementRef(name = "status", type = JAXBElement.class, required = false)
    protected JAXBElement<String> status;
    protected OutputWorker worker;

    /**
     * Gets the value of the wfmOrderId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getWfmOrderId() {
        return wfmOrderId;
    }

    /**
     * Sets the value of the wfmOrderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setWfmOrderId(JAXBElement<Long> value) {
        this.wfmOrderId = value;
    }

    /**
     * Gets the value of the wfmOrderNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getWfmOrderNumber() {
        return wfmOrderNumber;
    }

    /**
     * Sets the value of the wfmOrderNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setWfmOrderNumber(JAXBElement<Long> value) {
        this.wfmOrderNumber = value;
    }

    /**
     * Gets the value of the agreedStartTime property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAgreedStartTime() {
        return agreedStartTime;
    }

    /**
     * Sets the value of the agreedStartTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAgreedStartTime(JAXBElement<XMLGregorianCalendar> value) {
        this.agreedStartTime = value;
    }

    /**
     * Gets the value of the agreedFinishTime property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAgreedFinishTime() {
        return agreedFinishTime;
    }

    /**
     * Sets the value of the agreedFinishTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAgreedFinishTime(JAXBElement<XMLGregorianCalendar> value) {
        this.agreedFinishTime = value;
    }

    /**
     * Gets the value of the checkTime property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getCheckTime() {
        return checkTime;
    }

    /**
     * Sets the value of the checkTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setCheckTime(JAXBElement<XMLGregorianCalendar> value) {
        this.checkTime = value;
    }

    /**
     * Gets the value of the registerTime property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getRegisterTime() {
        return registerTime;
    }

    /**
     * Sets the value of the registerTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setRegisterTime(JAXBElement<XMLGregorianCalendar> value) {
        this.registerTime = value;
    }

    /**
     * Gets the value of the confirmed property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getConfirmed() {
        return confirmed;
    }

    /**
     * Sets the value of the confirmed property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setConfirmed(JAXBElement<Integer> value) {
        this.confirmed = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setStatus(JAXBElement<String> value) {
        this.status = value;
    }

    /**
     * Gets the value of the worker property.
     * 
     * @return
     *     possible object is
     *     {@link OutputWorker }
     *     
     */
    public OutputWorker getWorker() {
        return worker;
    }

    /**
     * Sets the value of the worker property.
     * 
     * @param value
     *     allowed object is
     *     {@link OutputWorker }
     *     
     */
    public void setWorker(OutputWorker value) {
        this.worker = value;
    }

}
