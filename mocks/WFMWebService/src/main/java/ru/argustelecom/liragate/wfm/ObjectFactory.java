
package ru.argustelecom.liragate.wfm;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.argustelecom.liragate.wfm package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AssignVisitIntervalResponce_QNAME = new QName("http://wfm.liragate.argustelecom.ru/", "assignVisitIntervalResponce");
    private final static QName _FindPossibleVisitIntervalResponce_QNAME = new QName("http://wfm.liragate.argustelecom.ru/", "findPossibleVisitIntervalResponce");
    private final static QName _GetVisitInfoResponce_QNAME = new QName("http://wfm.liragate.argustelecom.ru/", "getVisitInfoResponce");
    private final static QName _LiraAddress_QNAME = new QName("http://wfm.liragate.argustelecom.ru/", "liraAddress");
    private final static QName _LiraOrderIds_QNAME = new QName("http://wfm.liragate.argustelecom.ru/", "liraOrderIds");
    private final static QName _LiraOrders_QNAME = new QName("http://wfm.liragate.argustelecom.ru/", "liraOrders");
    private final static QName _OutputWorker_QNAME = new QName("http://wfm.liragate.argustelecom.ru/", "outputWorker");
    private final static QName _LiraOrderId_QNAME = new QName("http://wfm.liragate.argustelecom.ru/", "liraOrderId");
    private final static QName _OutputWfmOrderWfmOrderId_QNAME = new QName("", "wfmOrderId");
    private final static QName _OutputWfmOrderWfmOrderNumber_QNAME = new QName("", "wfmOrderNumber");
    private final static QName _OutputWfmOrderAgreedStartTime_QNAME = new QName("", "agreedStartTime");
    private final static QName _OutputWfmOrderAgreedFinishTime_QNAME = new QName("", "agreedFinishTime");
    private final static QName _OutputWfmOrderCheckTime_QNAME = new QName("", "checkTime");
    private final static QName _OutputWfmOrderRegisterTime_QNAME = new QName("", "registerTime");
    private final static QName _OutputWfmOrderConfirmed_QNAME = new QName("", "confirmed");
    private final static QName _OutputWfmOrderStatus_QNAME = new QName("", "status");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.argustelecom.liragate.wfm
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Result }
     * 
     */
    public Result createResult() {
        return new Result();
    }

    /**
     * Create an instance of {@link GetVisitInfoResponce }
     * 
     */
    public GetVisitInfoResponce createGetVisitInfoResponce() {
        return new GetVisitInfoResponce();
    }

    /**
     * Create an instance of {@link AssignVisitIntervalResponce }
     * 
     */
    public AssignVisitIntervalResponce createAssignVisitIntervalResponce() {
        return new AssignVisitIntervalResponce();
    }

    /**
     * Create an instance of {@link FindPossibleVisitIntervalResponce }
     * 
     */
    public FindPossibleVisitIntervalResponce createFindPossibleVisitIntervalResponce() {
        return new FindPossibleVisitIntervalResponce();
    }

    /**
     * Create an instance of {@link LiraAddress }
     * 
     */
    public LiraAddress createLiraAddress() {
        return new LiraAddress();
    }

    /**
     * Create an instance of {@link LiraOrderIds }
     * 
     */
    public LiraOrderIds createLiraOrderIds() {
        return new LiraOrderIds();
    }

    /**
     * Create an instance of {@link LiraOrders }
     * 
     */
    public LiraOrders createLiraOrders() {
        return new LiraOrders();
    }

    /**
     * Create an instance of {@link OutputWorker }
     * 
     */
    public OutputWorker createOutputWorker() {
        return new OutputWorker();
    }

    /**
     * Create an instance of {@link LiraOrder }
     * 
     */
    public LiraOrder createLiraOrder() {
        return new LiraOrder();
    }

    /**
     * Create an instance of {@link SimpleResponce }
     * 
     */
    public SimpleResponce createSimpleResponce() {
        return new SimpleResponce();
    }

    /**
     * Create an instance of {@link OutputWfmOrder }
     * 
     */
    public OutputWfmOrder createOutputWfmOrder() {
        return new OutputWfmOrder();
    }

    /**
     * Create an instance of {@link Interval }
     * 
     */
    public Interval createInterval() {
        return new Interval();
    }

    /**
     * Create an instance of {@link WfmWorksite }
     * 
     */
    public WfmWorksite createWfmWorksite() {
        return new WfmWorksite();
    }

    /**
     * Create an instance of {@link Result.Intervals }
     * 
     */
    public Result.Intervals createResultIntervals() {
        return new Result.Intervals();
    }

    /**
     * Create an instance of {@link GetVisitInfoResponce.WfmOrders }
     * 
     */
    public GetVisitInfoResponce.WfmOrders createGetVisitInfoResponceWfmOrders() {
        return new GetVisitInfoResponce.WfmOrders();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AssignVisitIntervalResponce }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wfm.liragate.argustelecom.ru/", name = "assignVisitIntervalResponce")
    public JAXBElement<AssignVisitIntervalResponce> createAssignVisitIntervalResponce(AssignVisitIntervalResponce value) {
        return new JAXBElement<AssignVisitIntervalResponce>(_AssignVisitIntervalResponce_QNAME, AssignVisitIntervalResponce.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindPossibleVisitIntervalResponce }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wfm.liragate.argustelecom.ru/", name = "findPossibleVisitIntervalResponce")
    public JAXBElement<FindPossibleVisitIntervalResponce> createFindPossibleVisitIntervalResponce(FindPossibleVisitIntervalResponce value) {
        return new JAXBElement<FindPossibleVisitIntervalResponce>(_FindPossibleVisitIntervalResponce_QNAME, FindPossibleVisitIntervalResponce.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVisitInfoResponce }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wfm.liragate.argustelecom.ru/", name = "getVisitInfoResponce")
    public JAXBElement<GetVisitInfoResponce> createGetVisitInfoResponce(GetVisitInfoResponce value) {
        return new JAXBElement<GetVisitInfoResponce>(_GetVisitInfoResponce_QNAME, GetVisitInfoResponce.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LiraAddress }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wfm.liragate.argustelecom.ru/", name = "liraAddress")
    public JAXBElement<LiraAddress> createLiraAddress(LiraAddress value) {
        return new JAXBElement<LiraAddress>(_LiraAddress_QNAME, LiraAddress.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LiraOrderIds }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wfm.liragate.argustelecom.ru/", name = "liraOrderIds")
    public JAXBElement<LiraOrderIds> createLiraOrderIds(LiraOrderIds value) {
        return new JAXBElement<LiraOrderIds>(_LiraOrderIds_QNAME, LiraOrderIds.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LiraOrders }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wfm.liragate.argustelecom.ru/", name = "liraOrders")
    public JAXBElement<LiraOrders> createLiraOrders(LiraOrders value) {
        return new JAXBElement<LiraOrders>(_LiraOrders_QNAME, LiraOrders.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutputWorker }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wfm.liragate.argustelecom.ru/", name = "outputWorker")
    public JAXBElement<OutputWorker> createOutputWorker(OutputWorker value) {
        return new JAXBElement<OutputWorker>(_OutputWorker_QNAME, OutputWorker.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wfm.liragate.argustelecom.ru/", name = "liraOrderId")
    public JAXBElement<String> createLiraOrderId(String value) {
        return new JAXBElement<String>(_LiraOrderId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "wfmOrderId", scope = OutputWfmOrder.class)
    public JAXBElement<Long> createOutputWfmOrderWfmOrderId(Long value) {
        return new JAXBElement<Long>(_OutputWfmOrderWfmOrderId_QNAME, Long.class, OutputWfmOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "wfmOrderNumber", scope = OutputWfmOrder.class)
    public JAXBElement<Long> createOutputWfmOrderWfmOrderNumber(Long value) {
        return new JAXBElement<Long>(_OutputWfmOrderWfmOrderNumber_QNAME, Long.class, OutputWfmOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "agreedStartTime", scope = OutputWfmOrder.class)
    public JAXBElement<XMLGregorianCalendar> createOutputWfmOrderAgreedStartTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OutputWfmOrderAgreedStartTime_QNAME, XMLGregorianCalendar.class, OutputWfmOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "agreedFinishTime", scope = OutputWfmOrder.class)
    public JAXBElement<XMLGregorianCalendar> createOutputWfmOrderAgreedFinishTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OutputWfmOrderAgreedFinishTime_QNAME, XMLGregorianCalendar.class, OutputWfmOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "checkTime", scope = OutputWfmOrder.class)
    public JAXBElement<XMLGregorianCalendar> createOutputWfmOrderCheckTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OutputWfmOrderCheckTime_QNAME, XMLGregorianCalendar.class, OutputWfmOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "registerTime", scope = OutputWfmOrder.class)
    public JAXBElement<XMLGregorianCalendar> createOutputWfmOrderRegisterTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OutputWfmOrderRegisterTime_QNAME, XMLGregorianCalendar.class, OutputWfmOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "confirmed", scope = OutputWfmOrder.class)
    public JAXBElement<Integer> createOutputWfmOrderConfirmed(Integer value) {
        return new JAXBElement<Integer>(_OutputWfmOrderConfirmed_QNAME, Integer.class, OutputWfmOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "status", scope = OutputWfmOrder.class)
    public JAXBElement<String> createOutputWfmOrderStatus(String value) {
        return new JAXBElement<String>(_OutputWfmOrderStatus_QNAME, String.class, OutputWfmOrder.class, value);
    }

}
