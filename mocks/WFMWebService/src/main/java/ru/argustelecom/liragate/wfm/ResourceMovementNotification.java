
package ru.argustelecom.liragate.wfm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ru.argustelecom.model.liragate.rms.TResourceInfo;


/**
 * <p>Java class for ResourceMovementNotification complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResourceMovementNotification"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="resourceInfo" type="{http://argustelecom.ru/model/liragate/rms}tResourceInfo"/&gt;
 *         &lt;element name="installed" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResourceMovementNotification", propOrder = {
    "resourceInfo",
    "installed"
})
public class ResourceMovementNotification {

    @XmlElement(required = true)
    protected TResourceInfo resourceInfo;
    protected int installed;

    /**
     * Gets the value of the resourceInfo property.
     * 
     * @return
     *     possible object is
     *     {@link TResourceInfo }
     *     
     */
    public TResourceInfo getResourceInfo() {
        return resourceInfo;
    }

    /**
     * Sets the value of the resourceInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TResourceInfo }
     *     
     */
    public void setResourceInfo(TResourceInfo value) {
        this.resourceInfo = value;
    }

    /**
     * Gets the value of the installed property.
     * 
     */
    public int getInstalled() {
        return installed;
    }

    /**
     * Sets the value of the installed property.
     * 
     */
    public void setInstalled(int value) {
        this.installed = value;
    }

}
