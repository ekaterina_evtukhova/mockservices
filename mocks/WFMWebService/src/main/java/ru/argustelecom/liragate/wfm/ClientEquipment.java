
package ru.argustelecom.liragate.wfm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ClientEquipment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClientEquipment"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="sn" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="eqCategory" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="eqType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="eqStatus" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="eqOwn" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="comment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClientEquipment", propOrder = {
    "sn",
    "eqCategory",
    "eqType",
    "eqStatus",
    "eqOwn",
    "comment"
})
public class ClientEquipment {

    @XmlElement(required = true)
    protected String sn;
    @XmlElement(required = true)
    protected String eqCategory;
    @XmlElement(required = true)
    protected String eqType;
    protected int eqStatus;
    @XmlElement(required = true)
    protected String eqOwn;
    protected String comment;

    /**
     * Gets the value of the sn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSn() {
        return sn;
    }

    /**
     * Sets the value of the sn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSn(String value) {
        this.sn = value;
    }

    /**
     * Gets the value of the eqCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEqCategory() {
        return eqCategory;
    }

    /**
     * Sets the value of the eqCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEqCategory(String value) {
        this.eqCategory = value;
    }

    /**
     * Gets the value of the eqType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEqType() {
        return eqType;
    }

    /**
     * Sets the value of the eqType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEqType(String value) {
        this.eqType = value;
    }

    /**
     * Gets the value of the eqStatus property.
     * 
     */
    public int getEqStatus() {
        return eqStatus;
    }

    /**
     * Sets the value of the eqStatus property.
     * 
     */
    public void setEqStatus(int value) {
        this.eqStatus = value;
    }

    /**
     * Gets the value of the eqOwn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEqOwn() {
        return eqOwn;
    }

    /**
     * Sets the value of the eqOwn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEqOwn(String value) {
        this.eqOwn = value;
    }

    /**
     * Gets the value of the comment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets the value of the comment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComment(String value) {
        this.comment = value;
    }

}
