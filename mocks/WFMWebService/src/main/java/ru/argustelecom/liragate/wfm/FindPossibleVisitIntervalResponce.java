
package ru.argustelecom.liragate.wfm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for findPossibleVisitIntervalResponce complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="findPossibleVisitIntervalResponce"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://wfm.liragate.argustelecom.ru/}simpleResponce"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="result" type="{http://wfm.liragate.argustelecom.ru/}result"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "findPossibleVisitIntervalResponce", propOrder = {
    "result"
})
public class FindPossibleVisitIntervalResponce
    extends SimpleResponce
{

    @XmlElement(required = true)
    protected Result result;

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link Result }
     *     
     */
    public Result getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link Result }
     *     
     */
    public void setResult(Result value) {
        this.result = value;
    }

}
