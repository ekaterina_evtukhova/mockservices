
package ru.argustelecom.liragate.wfm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ClientEquipmentList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClientEquipmentList"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="clientEq" type="{http://wfm.liragate.argustelecom.ru/}ClientEquipment" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClientEquipmentList", propOrder = {
    "clientEq"
})
public class ClientEquipmentList {

    @XmlElement(required = true)
    protected List<ClientEquipment> clientEq;

    /**
     * Gets the value of the clientEq property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the clientEq property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClientEq().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClientEquipment }
     * 
     * 
     */
    public List<ClientEquipment> getClientEq() {
        if (clientEq == null) {
            clientEq = new ArrayList<ClientEquipment>();
        }
        return this.clientEq;
    }

}
