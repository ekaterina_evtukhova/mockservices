
package ru.argustelecom.liragate.wfm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for liraAddress complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="liraAddress"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ku" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="nd" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="kw" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="korp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="et" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="pod" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="kr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "liraAddress", propOrder = {
    "ku",
    "nd",
    "kw",
    "korp",
    "et",
    "pod",
    "kr"
})
public class LiraAddress {

    protected int ku;
    @XmlElement(required = true)
    protected String nd;
    @XmlElement(required = true)
    protected String kw;
    protected String korp;
    protected Integer et;
    protected Integer pod;
    protected String kr;

    /**
     * Gets the value of the ku property.
     * 
     */
    public int getKu() {
        return ku;
    }

    /**
     * Sets the value of the ku property.
     * 
     */
    public void setKu(int value) {
        this.ku = value;
    }

    /**
     * Gets the value of the nd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNd() {
        return nd;
    }

    /**
     * Sets the value of the nd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNd(String value) {
        this.nd = value;
    }

    /**
     * Gets the value of the kw property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKw() {
        return kw;
    }

    /**
     * Sets the value of the kw property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKw(String value) {
        this.kw = value;
    }

    /**
     * Gets the value of the korp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKorp() {
        return korp;
    }

    /**
     * Sets the value of the korp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKorp(String value) {
        this.korp = value;
    }

    /**
     * Gets the value of the et property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEt() {
        return et;
    }

    /**
     * Sets the value of the et property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEt(Integer value) {
        this.et = value;
    }

    /**
     * Gets the value of the pod property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPod() {
        return pod;
    }

    /**
     * Sets the value of the pod property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPod(Integer value) {
        this.pod = value;
    }

    /**
     * Gets the value of the kr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKr() {
        return kr;
    }

    /**
     * Sets the value of the kr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKr(String value) {
        this.kr = value;
    }

}
