
package ru.argustelecom.liragate.wfm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Client complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Client"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="clientPhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="abon" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="address" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="nls" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="nDog" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="clientEq" type="{http://wfm.liragate.argustelecom.ru/}ClientEquipment" minOccurs="0"/&gt;
 *         &lt;element name="applicationList" type="{http://wfm.liragate.argustelecom.ru/}LiraOrderList"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Client", propOrder = {
    "clientPhoneNumber",
    "abon",
    "name",
    "address",
    "nls",
    "nDog",
    "clientEq",
    "applicationList"
})
public class Client {

    @XmlElement(required = true)
    protected String clientPhoneNumber;
    @XmlElement(required = true)
    protected String abon;
    protected String name;
    @XmlElement(required = true)
    protected String address;
    @XmlElement(required = true)
    protected String nls;
    @XmlElement(required = true)
    protected String nDog;
    protected ClientEquipment clientEq;
    @XmlElement(required = true)
    protected LiraOrderList applicationList;

    /**
     * Gets the value of the clientPhoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientPhoneNumber() {
        return clientPhoneNumber;
    }

    /**
     * Sets the value of the clientPhoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientPhoneNumber(String value) {
        this.clientPhoneNumber = value;
    }

    /**
     * Gets the value of the abon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAbon() {
        return abon;
    }

    /**
     * Sets the value of the abon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAbon(String value) {
        this.abon = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress(String value) {
        this.address = value;
    }

    /**
     * Gets the value of the nls property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNls() {
        return nls;
    }

    /**
     * Sets the value of the nls property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNls(String value) {
        this.nls = value;
    }

    /**
     * Gets the value of the nDog property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNDog() {
        return nDog;
    }

    /**
     * Sets the value of the nDog property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNDog(String value) {
        this.nDog = value;
    }

    /**
     * Gets the value of the clientEq property.
     * 
     * @return
     *     possible object is
     *     {@link ClientEquipment }
     *     
     */
    public ClientEquipment getClientEq() {
        return clientEq;
    }

    /**
     * Sets the value of the clientEq property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClientEquipment }
     *     
     */
    public void setClientEq(ClientEquipment value) {
        this.clientEq = value;
    }

    /**
     * Gets the value of the applicationList property.
     * 
     * @return
     *     possible object is
     *     {@link LiraOrderList }
     *     
     */
    public LiraOrderList getApplicationList() {
        return applicationList;
    }

    /**
     * Sets the value of the applicationList property.
     * 
     * @param value
     *     allowed object is
     *     {@link LiraOrderList }
     *     
     */
    public void setApplicationList(LiraOrderList value) {
        this.applicationList = value;
    }

}
