
package ru.argustelecom.liragate.wfm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for wfmWorksite complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="wfmWorksite"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="worksiteId" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="worksiteName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wfmWorksite", propOrder = {
    "worksiteId",
    "worksiteName"
})
public class WfmWorksite {

    protected long worksiteId;
    @XmlElement(required = true)
    protected String worksiteName;

    /**
     * Gets the value of the worksiteId property.
     * 
     */
    public long getWorksiteId() {
        return worksiteId;
    }

    /**
     * Sets the value of the worksiteId property.
     * 
     */
    public void setWorksiteId(long value) {
        this.worksiteId = value;
    }

    /**
     * Gets the value of the worksiteName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorksiteName() {
        return worksiteName;
    }

    /**
     * Sets the value of the worksiteName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorksiteName(String value) {
        this.worksiteName = value;
    }

}
