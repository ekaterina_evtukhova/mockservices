
package ru.argustelecom.model.rms;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for iStorage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="iStorage"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://argustelecom.ru/model/rms}iAccount"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="parentStorageId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="responsibleWorkerId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="buildingId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "iStorage", propOrder = {
    "parentStorageId",
    "responsibleWorkerId",
    "buildingId"
})
public class IStorage
    extends IAccount
{

    protected Long parentStorageId;
    protected Long responsibleWorkerId;
    protected Long buildingId;

    /**
     * Gets the value of the parentStorageId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getParentStorageId() {
        return parentStorageId;
    }

    /**
     * Sets the value of the parentStorageId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setParentStorageId(Long value) {
        this.parentStorageId = value;
    }

    /**
     * Gets the value of the responsibleWorkerId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getResponsibleWorkerId() {
        return responsibleWorkerId;
    }

    /**
     * Sets the value of the responsibleWorkerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setResponsibleWorkerId(Long value) {
        this.responsibleWorkerId = value;
    }

    /**
     * Gets the value of the buildingId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getBuildingId() {
        return buildingId;
    }

    /**
     * Sets the value of the buildingId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setBuildingId(Long value) {
        this.buildingId = value;
    }

}
