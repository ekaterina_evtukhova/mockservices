
package ru.argustelecom.model.rms;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import ru.argustelecom.model.base.IAbstractEntity;
import ru.argustelecom.model.resource.IResourceTypeFilter;


/**
 * <p>Java class for iResourceRequirement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="iResourceRequirement"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://argustelecom.ru/model/base}iAbstractEntity"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="resourceOrderId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="filter" type="{http://argustelecom.ru/model/resource}iResourceTypeFilter" minOccurs="0"/&gt;
 *         &lt;element name="transferTerms" type="{http://argustelecom.ru/model/rms}iTransferTerms" minOccurs="0"/&gt;
 *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "iResourceRequirement", propOrder = {
    "resourceOrderId",
    "filter",
    "transferTerms",
    "amount"
})
public class IResourceRequirement
    extends IAbstractEntity
{

    protected Long resourceOrderId;
    protected IResourceTypeFilter filter;
    protected ITransferTerms transferTerms;
    protected Long amount;

    /**
     * Gets the value of the resourceOrderId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getResourceOrderId() {
        return resourceOrderId;
    }

    /**
     * Sets the value of the resourceOrderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setResourceOrderId(Long value) {
        this.resourceOrderId = value;
    }

    /**
     * Gets the value of the filter property.
     * 
     * @return
     *     possible object is
     *     {@link IResourceTypeFilter }
     *     
     */
    public IResourceTypeFilter getFilter() {
        return filter;
    }

    /**
     * Sets the value of the filter property.
     * 
     * @param value
     *     allowed object is
     *     {@link IResourceTypeFilter }
     *     
     */
    public void setFilter(IResourceTypeFilter value) {
        this.filter = value;
    }

    /**
     * Gets the value of the transferTerms property.
     * 
     * @return
     *     possible object is
     *     {@link ITransferTerms }
     *     
     */
    public ITransferTerms getTransferTerms() {
        return transferTerms;
    }

    /**
     * Sets the value of the transferTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link ITransferTerms }
     *     
     */
    public void setTransferTerms(ITransferTerms value) {
        this.transferTerms = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAmount(Long value) {
        this.amount = value;
    }

}
