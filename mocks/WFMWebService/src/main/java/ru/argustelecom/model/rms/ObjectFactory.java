
package ru.argustelecom.model.rms;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.argustelecom.model.rms package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.argustelecom.model.rms
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ITransferType }
     * 
     */
    public ITransferType createITransferType() {
        return new ITransferType();
    }

    /**
     * Create an instance of {@link ITransferTerms }
     * 
     */
    public ITransferTerms createITransferTerms() {
        return new ITransferTerms();
    }

    /**
     * Create an instance of {@link ITransferReason }
     * 
     */
    public ITransferReason createITransferReason() {
        return new ITransferReason();
    }

    /**
     * Create an instance of {@link IAccount }
     * 
     */
    public IAccount createIAccount() {
        return new IAccount();
    }

    /**
     * Create an instance of {@link IAccountScope }
     * 
     */
    public IAccountScope createIAccountScope() {
        return new IAccountScope();
    }

    /**
     * Create an instance of {@link IConsumer }
     * 
     */
    public IConsumer createIConsumer() {
        return new IConsumer();
    }

    /**
     * Create an instance of {@link IContractor }
     * 
     */
    public IContractor createIContractor() {
        return new IContractor();
    }

    /**
     * Create an instance of {@link IStorage }
     * 
     */
    public IStorage createIStorage() {
        return new IStorage();
    }

    /**
     * Create an instance of {@link IWorkerBag }
     * 
     */
    public IWorkerBag createIWorkerBag() {
        return new IWorkerBag();
    }

    /**
     * Create an instance of {@link IResourceRequirement }
     * 
     */
    public IResourceRequirement createIResourceRequirement() {
        return new IResourceRequirement();
    }

}
