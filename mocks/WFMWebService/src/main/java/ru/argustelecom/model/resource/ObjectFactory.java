
package ru.argustelecom.model.resource;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.argustelecom.model.resource package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.argustelecom.model.resource
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link IResource }
     * 
     */
    public IResource createIResource() {
        return new IResource();
    }

    /**
     * Create an instance of {@link IResourceType }
     * 
     */
    public IResourceType createIResourceType() {
        return new IResourceType();
    }

    /**
     * Create an instance of {@link IVendor }
     * 
     */
    public IVendor createIVendor() {
        return new IVendor();
    }

    /**
     * Create an instance of {@link IResourceCategory }
     * 
     */
    public IResourceCategory createIResourceCategory() {
        return new IResourceCategory();
    }

    /**
     * Create an instance of {@link IResourceTypeFilter }
     * 
     */
    public IResourceTypeFilter createIResourceTypeFilter() {
        return new IResourceTypeFilter();
    }

    /**
     * Create an instance of {@link IFunctionalFeature }
     * 
     */
    public IFunctionalFeature createIFunctionalFeature() {
        return new IFunctionalFeature();
    }

}
