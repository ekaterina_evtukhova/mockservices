
package ru.argustelecom.model.liragate.rms;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ru.argustelecom.model.resource.IResource;
import ru.argustelecom.model.rms.IAccount;
import ru.argustelecom.model.rms.ITransferReason;
import ru.argustelecom.model.rms.ITransferTerms;
import ru.argustelecom.model.rms.ITransferType;


/**
 * <p>Java class for tResourceInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tResourceInfo"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="resource" type="{http://argustelecom.ru/model/resource}iResource"/&gt;
 *         &lt;element name="macAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="warranty" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="used" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="requiredAction" type="{http://argustelecom.ru/model/rms}iTransferType" minOccurs="0"/&gt;
 *         &lt;element name="transferTerms" type="{http://argustelecom.ru/model/rms}iTransferTerms" minOccurs="0"/&gt;
 *         &lt;element name="transferReason" type="{http://argustelecom.ru/model/rms}iTransferReason" minOccurs="0"/&gt;
 *         &lt;element name="transferPrice" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/&gt;
 *         &lt;element name="storage" type="{http://argustelecom.ru/model/rms}iAccount" minOccurs="0"/&gt;
 *         &lt;element name="client" type="{http://argustelecom.ru/model/liragate/rms}tClient" minOccurs="0"/&gt;
 *         &lt;element name="tl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tResourceInfo", propOrder = {
    "resource",
    "macAddress",
    "status",
    "warranty",
    "used",
    "requiredAction",
    "transferTerms",
    "transferReason",
    "transferPrice",
    "storage",
    "client",
    "tl"
})
public class TResourceInfo {

    @XmlElement(required = true)
    protected IResource resource;
    protected String macAddress;
    @XmlElement(required = true)
    protected String status;
    protected String warranty;
    protected boolean used;
    protected ITransferType requiredAction;
    protected ITransferTerms transferTerms;
    protected ITransferReason transferReason;
    protected Double transferPrice;
    protected IAccount storage;
    protected TClient client;
    protected String tl;

    /**
     * Gets the value of the resource property.
     * 
     * @return
     *     possible object is
     *     {@link IResource }
     *     
     */
    public IResource getResource() {
        return resource;
    }

    /**
     * Sets the value of the resource property.
     * 
     * @param value
     *     allowed object is
     *     {@link IResource }
     *     
     */
    public void setResource(IResource value) {
        this.resource = value;
    }

    /**
     * Gets the value of the macAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMacAddress() {
        return macAddress;
    }

    /**
     * Sets the value of the macAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMacAddress(String value) {
        this.macAddress = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the warranty property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWarranty() {
        return warranty;
    }

    /**
     * Sets the value of the warranty property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWarranty(String value) {
        this.warranty = value;
    }

    /**
     * Gets the value of the used property.
     * 
     */
    public boolean isUsed() {
        return used;
    }

    /**
     * Sets the value of the used property.
     * 
     */
    public void setUsed(boolean value) {
        this.used = value;
    }

    /**
     * Gets the value of the requiredAction property.
     * 
     * @return
     *     possible object is
     *     {@link ITransferType }
     *     
     */
    public ITransferType getRequiredAction() {
        return requiredAction;
    }

    /**
     * Sets the value of the requiredAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link ITransferType }
     *     
     */
    public void setRequiredAction(ITransferType value) {
        this.requiredAction = value;
    }

    /**
     * Gets the value of the transferTerms property.
     * 
     * @return
     *     possible object is
     *     {@link ITransferTerms }
     *     
     */
    public ITransferTerms getTransferTerms() {
        return transferTerms;
    }

    /**
     * Sets the value of the transferTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link ITransferTerms }
     *     
     */
    public void setTransferTerms(ITransferTerms value) {
        this.transferTerms = value;
    }

    /**
     * Gets the value of the transferReason property.
     * 
     * @return
     *     possible object is
     *     {@link ITransferReason }
     *     
     */
    public ITransferReason getTransferReason() {
        return transferReason;
    }

    /**
     * Sets the value of the transferReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link ITransferReason }
     *     
     */
    public void setTransferReason(ITransferReason value) {
        this.transferReason = value;
    }

    /**
     * Gets the value of the transferPrice property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getTransferPrice() {
        return transferPrice;
    }

    /**
     * Sets the value of the transferPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setTransferPrice(Double value) {
        this.transferPrice = value;
    }

    /**
     * Gets the value of the storage property.
     * 
     * @return
     *     possible object is
     *     {@link IAccount }
     *     
     */
    public IAccount getStorage() {
        return storage;
    }

    /**
     * Sets the value of the storage property.
     * 
     * @param value
     *     allowed object is
     *     {@link IAccount }
     *     
     */
    public void setStorage(IAccount value) {
        this.storage = value;
    }

    /**
     * Gets the value of the client property.
     * 
     * @return
     *     possible object is
     *     {@link TClient }
     *     
     */
    public TClient getClient() {
        return client;
    }

    /**
     * Sets the value of the client property.
     * 
     * @param value
     *     allowed object is
     *     {@link TClient }
     *     
     */
    public void setClient(TClient value) {
        this.client = value;
    }

    /**
     * Gets the value of the tl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTl() {
        return tl;
    }

    /**
     * Sets the value of the tl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTl(String value) {
        this.tl = value;
    }

}
