
package ru.argustelecom.model.resource;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import ru.argustelecom.model.base.IAbstractDirectory;


/**
 * <p>Java class for iVendor complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="iVendor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://argustelecom.ru/model/base}iAbstractDirectory"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "iVendor")
public class IVendor
    extends IAbstractDirectory
{


}
