
package ru.argustelecom.model.rms;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import ru.argustelecom.model.base.IAbstractDirectory;


/**
 * <p>Java class for iTransferTerms complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="iTransferTerms"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://argustelecom.ru/model/base}iAbstractDirectory"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="rent" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="fullPricePurchase" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "iTransferTerms", propOrder = {
    "rent",
    "fullPricePurchase"
})
public class ITransferTerms
    extends IAbstractDirectory
{

    protected Boolean rent;
    protected Boolean fullPricePurchase;

    /**
     * Gets the value of the rent property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRent() {
        return rent;
    }

    /**
     * Sets the value of the rent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRent(Boolean value) {
        this.rent = value;
    }

    /**
     * Gets the value of the fullPricePurchase property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFullPricePurchase() {
        return fullPricePurchase;
    }

    /**
     * Sets the value of the fullPricePurchase property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFullPricePurchase(Boolean value) {
        this.fullPricePurchase = value;
    }

}
