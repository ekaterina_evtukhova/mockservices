
package ru.argustelecom.model.liragate.rms;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.argustelecom.model.liragate.rms package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Return_QNAME = new QName("http://argustelecom.ru/model/liragate/rms", "return");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.argustelecom.model.liragate.rms
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Response }
     * 
     */
    public Response createResponse() {
        return new Response();
    }

    /**
     * Create an instance of {@link TResourceInfo }
     * 
     */
    public TResourceInfo createTResourceInfo() {
        return new TResourceInfo();
    }

    /**
     * Create an instance of {@link TClient }
     * 
     */
    public TClient createTClient() {
        return new TClient();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Response }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://argustelecom.ru/model/liragate/rms", name = "return")
    public JAXBElement<Response> createReturn(Response value) {
        return new JAXBElement<Response>(_Return_QNAME, Response.class, null, value);
    }

}
