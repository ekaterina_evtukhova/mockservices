
package ru.argustelecom.model.rms;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import ru.argustelecom.model.base.IAbstractDirectory;


/**
 * <p>Java class for iTransferReason complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="iTransferReason"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://argustelecom.ru/model/base}iAbstractDirectory"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="transferTypeId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "iTransferReason", propOrder = {
    "transferTypeId"
})
public class ITransferReason
    extends IAbstractDirectory
{

    protected Long transferTypeId;

    /**
     * Gets the value of the transferTypeId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getTransferTypeId() {
        return transferTypeId;
    }

    /**
     * Sets the value of the transferTypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setTransferTypeId(Long value) {
        this.transferTypeId = value;
    }

}
