
package ru.argustelecom.model.base;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import ru.argustelecom.model.resource.IFunctionalFeature;
import ru.argustelecom.model.resource.IVendor;
import ru.argustelecom.model.rms.IAccountScope;
import ru.argustelecom.model.rms.ITransferReason;
import ru.argustelecom.model.rms.ITransferTerms;
import ru.argustelecom.model.rms.ITransferType;


/**
 * <p>Java class for iAbstractDirectory complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="iAbstractDirectory"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://argustelecom.ru/model/base}iAbstractEntity"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "iAbstractDirectory", propOrder = {
    "description"
})
@XmlSeeAlso({
    IVendor.class,
    IFunctionalFeature.class,
    ITransferType.class,
    ITransferTerms.class,
    ITransferReason.class,
    IAccountScope.class
})
public abstract class IAbstractDirectory
    extends IAbstractEntity
{

    protected String description;

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

}
