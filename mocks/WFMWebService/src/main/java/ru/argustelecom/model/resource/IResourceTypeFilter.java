
package ru.argustelecom.model.resource;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for iResourceTypeFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="iResourceTypeFilter"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element name="resourceType" type="{http://argustelecom.ru/model/resource}iResourceType"/&gt;
 *           &lt;element name="resourceCategory" type="{http://argustelecom.ru/model/resource}iResourceCategory"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="functionalFeature" type="{http://argustelecom.ru/model/resource}iFunctionalFeature" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "iResourceTypeFilter", propOrder = {
    "resourceType",
    "resourceCategory",
    "functionalFeature"
})
public class IResourceTypeFilter {

    protected IResourceType resourceType;
    protected IResourceCategory resourceCategory;
    protected List<IFunctionalFeature> functionalFeature;

    /**
     * Gets the value of the resourceType property.
     * 
     * @return
     *     possible object is
     *     {@link IResourceType }
     *     
     */
    public IResourceType getResourceType() {
        return resourceType;
    }

    /**
     * Sets the value of the resourceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link IResourceType }
     *     
     */
    public void setResourceType(IResourceType value) {
        this.resourceType = value;
    }

    /**
     * Gets the value of the resourceCategory property.
     * 
     * @return
     *     possible object is
     *     {@link IResourceCategory }
     *     
     */
    public IResourceCategory getResourceCategory() {
        return resourceCategory;
    }

    /**
     * Sets the value of the resourceCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link IResourceCategory }
     *     
     */
    public void setResourceCategory(IResourceCategory value) {
        this.resourceCategory = value;
    }

    /**
     * Gets the value of the functionalFeature property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the functionalFeature property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFunctionalFeature().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IFunctionalFeature }
     * 
     * 
     */
    public List<IFunctionalFeature> getFunctionalFeature() {
        if (functionalFeature == null) {
            functionalFeature = new ArrayList<IFunctionalFeature>();
        }
        return this.functionalFeature;
    }

}
