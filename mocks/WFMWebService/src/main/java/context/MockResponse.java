package context;

import com.fasterxml.jackson.databind.node.ObjectNode;
import ru.deltasolutions.oms.smart.mocker.mockinterface.requests.SetResponseRequest;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Katy on 12.04.2016.
 */
public class MockResponse {
    private LinkedHashMap<String, List<ObjectNode>> statResponses;

    public Integer getStatSyncResponsesCountByMethod(String method) {
        return getStatSyncResponseByMethod(method).size();
    }

    public LinkedHashMap<String, List<ObjectNode>> getStatSyncResponses() {
        return statResponses;
    }

    public List<ObjectNode> getStatSyncResponseByMethod(String method) {
        return statResponses.get(method);
    }

    public ObjectNode getStatSyncResponse(String method, Integer i) {
        return getStatSyncResponseByMethod(method).get(i);
    }

    public void setStatResponses(SetResponseRequest setResponseRequest) {
        statResponses = new LinkedHashMap<>();
        if (setResponseRequest.getRequests() == null)
            return;
        for (String method : setResponseRequest.getRequests().keySet()) {

            List<ObjectNode> objectNodes = setResponseRequest.getRequests().get(method);
            statResponses.put(method, objectNodes);
        }
    }


}
