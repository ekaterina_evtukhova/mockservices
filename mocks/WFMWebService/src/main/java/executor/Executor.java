package executor;

import org.apache.log4j.Logger;
import ru.deltasolutions.om.rt.wfm.webservice.WfmCallbackImpl;
import ru.argustelecom.liragate.wfm.ServiceInstallationResults;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by Katy on 23.03.2016.
 */
public class Executor {
    private static final ScheduledExecutorService scheduledExecutorService =
            Executors.newSingleThreadScheduledExecutor();
    private static Logger log = Logger.getLogger(Executor.class);

    public static void schedule(String liraOrderId, String time, ServiceInstallationResults liraOrder) {
        log.debug("Schedule orderId " + liraOrderId);
        WfmCallbackImpl wfmCallback = new WfmCallbackImpl(liraOrderId, liraOrder);
        scheduledExecutorService.schedule(wfmCallback,
                Long.valueOf(time), TimeUnit.SECONDS);
    }
}
