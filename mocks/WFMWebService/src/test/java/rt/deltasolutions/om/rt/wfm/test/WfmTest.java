package rt.deltasolutions.om.rt.wfm.test;

import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;
import org.junit.runner.RunWith;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;
import ru.argustelecom.liragate.wfm.*;
import ru.deltasolutions.oms.smart.mocker.mocker.annotation.BloodyTest;
import ru.deltasolutions.oms.smart.mocker.mocker.runner.BloodyMockerTestRunner;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Katy on 12.04.2016.
 */
@RunWith(BloodyMockerTestRunner.class)
public class WfmTest {
    private Object wfmLock = new Object();
    private volatile WfmApiForLiraAdapter wfmApiForLira;

    @BloodyTest(pathToConfig = "wfmTestFile.json")
    public void wfmTest() {
        sendConfirmVisit();
        sendUpdateVisitStatus();
        sendUpdateVisit();
    }

    @BloodyTest(pathToConfig = "wfmTestFile.json")
    public void wfmTes2t() {

    }

    private void sendConfirmVisit()
    {
        String liraOrderId = "78005530";
        String status = "WORKS_PERFORMED";
        try {
            SimpleResponce simpleResponce = getLiraApiForWfm().confirmVisit(liraOrderId, status);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private void sendUpdateVisit()
    {
        String liraOrderId = "78005530";
        String originator = "OMS";

        LiraOrders liraOrderList = new LiraOrders();

        LiraOrder liraOrder = new LiraOrder();
        liraOrder.setNz(78005531);
        liraOrder.setKkt(93);
        liraOrder.setTl("143522110918");
        liraOrder.setTl1("");
        liraOrder.setTl2("77459999010");
        liraOrder.setCount(0L);

        liraOrderList.addOrder(liraOrder);

        liraOrder = new LiraOrder();
        liraOrder.setNz(78005530);
        liraOrder.setKkt(171);
        liraOrder.setTl("143522110918");
        liraOrder.setTl1("");
        liraOrder.setTl2("143522110918");
        liraOrder.setCount(0L);

        liraOrderList.addOrder(liraOrder);

        try {
            SimpleResponce simpleResponce = getLiraApiForWfm().updateVisit(liraOrderId, liraOrderList, originator);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private void sendUpdateVisitStatus()
    {
        String liraOrderId = "78005530";
        Integer decision = -1;

        Long time = 1375315200000L;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String date = sdf.format(new Date(time));

        XMLGregorianCalendar xmlDate2 = null;
        try {
            xmlDate2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(date);
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }


        try {
            SimpleResponce simpleResponce = getLiraApiForWfm().updateVisitStatus(liraOrderId, decision, xmlDate2);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private WfmApiForLiraAdapter getLiraApiForWfm() throws MalformedURLException {
        if (wfmApiForLira == null) {
            synchronized (wfmLock) {
                if (wfmApiForLira == null) {
                    wfmApiForLira = getWfmApi();
                }
            }
        }
        return wfmApiForLira;
    }

    private WfmApiForLiraAdapter getWfmApi() throws MalformedURLException {
        URL wsdlLocation = WfmApiForLiraAdapterService.class
                .getResource("/META-INF/wsdl/oms-wfm-south.wsdl");

        WfmApiForLiraAdapterService service = new WfmApiForLiraAdapterService(
                new URL ("http://localhost:8080/WFMWebService-1.0.0-SNAPSHOT/WfmApiForLiraAdapter?wsdl"),
                new QName("http://webservice.wfm.rt.om.deltasolutions.rt/", "WfmApiForLiraAdapterService"));
        WfmApiForLiraAdapter ret_service = service.getWfmApiForLiraAdapterPort();
        BindingProvider bindingProvider = (BindingProvider) ret_service;
        bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
                "http://localhost:8080/WFMWebService-1.0.0-SNAPSHOT/WfmApiForLiraAdapter");
        return (WfmApiForLiraAdapter) bindingProvider;
    }
}
