package context;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by Katy on 15.03.2016.
 */
public class ApplicationContext {
    private static ApplicationContext instance;
    static Map<String, String> properties = new HashMap<String, String>();

    private ApplicationContext() {
        String configFileName = "config.properties";

        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {
            Properties prop = new Properties();

            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("Property file '" + configFileName + "' not found");
            }

            properties.put("group", prop.getProperty("group"));
            properties.put("code", prop.getProperty("code"));
            properties.put("description", prop.getProperty("description"));
            properties.put("systemId", prop.getProperty("systemId"));
            properties.put("comment", prop.getProperty("comment"));
            properties.put("resultCodeLTSN", prop.getProperty("resultCodeLTSN"));

            properties.put("scheduleTimeLTSN", prop.getProperty("scheduleTimeLTSN"));
            properties.put("scheduleTimeGOS", prop.getProperty("scheduleTimeGOS"));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getPropertyByName(String name) {
        if (instance == null) {
            instance = new ApplicationContext();
        }
        return properties.get(name);
    }
}
