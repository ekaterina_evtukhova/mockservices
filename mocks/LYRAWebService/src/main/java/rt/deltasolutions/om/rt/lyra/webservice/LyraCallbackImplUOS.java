package rt.deltasolutions.om.rt.lyra.webservice;

import org.apache.log4j.Logger;
import ru.deltasolutions.om.rt.lyraomsservice.*;

/**
 * Created by Katy on 01.04.2016.
 */
public class LyraCallbackImplUOS implements LyraCallback {
    private String componentId;
    private String orderId;
    private String operation;
    private Parameters parameters;

    private static Logger log = Logger.getLogger(LyraCallbackImplUOS.class);

    public LyraCallbackImplUOS(String componentId, String orderId, String operation, Parameters parameters) {
        this.componentId = componentId;
        this.orderId = orderId;
        this.operation = operation;
        this.parameters = parameters;
    }

    @Override
    public void run() {
        try {
            sendAsyncResponse();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendAsyncResponse() {
        log.debug("Send async SubmitLyraOrder start");
        LyraOmsService lyraOmsService = new LyraOmsService();
        LyraOmsServicePortType lyraOmsServicePortType = lyraOmsService.getLyraOmsServiceSOAP();

        UpdateOrderStatusRequest updateOrderStatusRequest = new UpdateOrderStatusRequest();
        updateOrderStatusRequest.setOrderId(orderId);
        updateOrderStatusRequest.setComponentId(componentId);
        updateOrderStatusRequest.setOperation(operation);

        UpdateOrderStatusResponse updateOrderStatusResponse =
                lyraOmsServicePortType.updateOrderStatus(updateOrderStatusRequest);

        System.out.println(updateOrderStatusRequest);
    }

}
