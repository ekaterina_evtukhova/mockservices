package rt.deltasolutions.om.rt.lyra.webservice;

import context.ApplicationContext;
import org.apache.log4j.Logger;
import parameters.AsyncParameters;
import parameters.RequestParameters;
import ru.deltasolutions.om.rt.lyraomsservice.*;
import ru.deltasolutions.om.rt.lyraomsservice.Parameters;

/**
 * Created by Katy on 23.03.2016.
 */
public class LyraCallbackImplLTSN implements LyraCallback {
    private String componentId;
    private String requestId;
    private String orderId;
    private String lyraTaskId;
    private String resultCode;
    private String resultText;
    private Parameters parameters;

    private static Logger log = Logger.getLogger(LyraCallbackImplLTSN.class);


    public LyraCallbackImplLTSN(String componentId, String requestId, String orderId, String lyraTaskId,
            String resultCode, String resultText, Parameters parameters) {
        this.componentId = componentId;
        this.requestId = requestId;
        this.orderId = orderId;
        this.lyraTaskId = lyraTaskId;
        this.resultCode = resultCode;
        this.resultText = resultText;
        this.parameters = parameters;
    }

    public void run() {
        try {
            sendAsyncResponse();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendAsyncResponse() {
        log.debug("Send async LyraTaskStatusNotification start");
        LyraOmsService lyraOmsService = new LyraOmsService();
        LyraOmsServicePortType lyraOmsServicePortType = lyraOmsService.getLyraOmsServiceSOAP();

        log.debug("Get parameters");
        LyraTaskStatusNotificationRequest lyraTaskStatusNotificationRequest = new LyraTaskStatusNotificationRequest();
        lyraTaskStatusNotificationRequest.setComponentId(componentId);
        lyraTaskStatusNotificationRequest.setLyraTaskId(lyraTaskId);
        lyraTaskStatusNotificationRequest.setOrderId(orderId);
        lyraTaskStatusNotificationRequest.setRequestId(requestId);
        lyraTaskStatusNotificationRequest.setOrderId(orderId);
        lyraTaskStatusNotificationRequest.setResultCode(resultCode);
        lyraTaskStatusNotificationRequest.setResultText(resultText);

        LyraTaskStatusNotificationResponse lyraTaskStatusNotificationResponse =
                lyraOmsServicePortType.lyraTaskStatusNotification(lyraTaskStatusNotificationRequest);

        System.out.println(lyraTaskStatusNotificationResponse);

        RequestParameters.getInstance().deleteParametersByComponentId(componentId);
    }
}
