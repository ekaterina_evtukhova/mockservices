package rt.deltasolutions.om.rt.lyra.webservice;

import org.apache.log4j.Logger;
import ru.deltasolutions.om.rt.lyraomsservice.*;

/**
 * Created by Katy on 01.04.2016.
 */
public class LyraCallbackImplSO implements LyraCallback {
    private Components components;
    private CommonData commonData;

    private static Logger log = Logger.getLogger(LyraCallbackImplSO.class);

    public LyraCallbackImplSO(Components components, CommonData commonData) {
        this.components = components;
        this.commonData = commonData;
    }

    @Override
    public void run() {
        try {
            sendAsyncResponse();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendAsyncResponse() {
        log.debug("Send async SubmitLyraOrder start");
        LyraOmsService lyraOmsService = new LyraOmsService();
        LyraOmsServicePortType lyraOmsServicePortType = lyraOmsService.getLyraOmsServiceSOAP();

        SubmitOrderRequest submitOrderRequest = new SubmitOrderRequest();
        submitOrderRequest.setCommonData(commonData);
        submitOrderRequest.setComponents(components);

        SubmitOrderResponse submitOrderResponse = lyraOmsServicePortType.submitOrder(submitOrderRequest);

        System.out.println(submitOrderResponse);
    }
}
