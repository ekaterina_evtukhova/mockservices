package rt.deltasolutions.om.rt.lyra.webservice;

import parameters.RequestParameters;
import ru.deltasolutions.om.rt.lyraomsservice.GetOrderStatusRequest;
import ru.deltasolutions.om.rt.lyraomsservice.GetOrderStatusResponse;
import ru.deltasolutions.om.rt.lyraomsservice.LyraOmsService;
import ru.deltasolutions.om.rt.lyraomsservice.LyraOmsServicePortType;

/**
 * Created by Katy on 01.04.2016.
 */
public class LyraCallbackImplGOS implements LyraCallback {
    private String componentId;
    private String orderId;

    public LyraCallbackImplGOS(String componentId, String orderId) {
        this.componentId = componentId;
        this.orderId = orderId;
    }

    @Override
    public void run() {

    }

    public void sendAsyncResponse() {
        LyraOmsService lyraOmsService = new LyraOmsService();
        LyraOmsServicePortType lyraOmsServicePortType = lyraOmsService.getLyraOmsServiceSOAP();

        GetOrderStatusRequest getOrderStatusRequest = new GetOrderStatusRequest();
        //params
        getOrderStatusRequest.setComponentId(componentId);
        getOrderStatusRequest.setOrderId(orderId);

        GetOrderStatusResponse getOrderStatusResponse = lyraOmsServicePortType.getOrderStatus(getOrderStatusRequest);
        System.out.println(getOrderStatusResponse);

    }
}
