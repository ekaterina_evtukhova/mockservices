package rt.deltasolutions.om.rt.lyra.webservice;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Created by Katy on 18.04.2016.
 */
@ApplicationPath("/")
public class App  extends Application {
}
