package rt.deltasolutions.om.rt.lyra.webservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import context.MockResponse;
import executor.Executor;
import org.apache.log4j.Logger;
import ru.deltasolutions.om.rt.lyra.service.*;
import ru.deltasolutions.om.rt.lyra.service.Components;
import ru.deltasolutions.om.rt.lyra.service.Error;
import ru.deltasolutions.om.rt.lyra.service.Parameters;
import ru.deltasolutions.om.rt.lyraomsservice.*;
import ru.deltasolutions.oms.smart.mocker.mockinterface.MockInterface;
import ru.deltasolutions.oms.smart.mocker.mockinterface.requests.SetResponseRequest;
import ru.deltasolutions.oms.smart.mocker.mockinterface.responses.AssertInfo;

import javax.jws.WebService;
import javax.ws.rs.Path;
import java.util.*;

/**
 * Created by Katy on 11.03.2016.
 */

@WebService(endpointInterface = "ru.deltasolutions.om.rt.lyra.service.OmsLyraServiceSoap")
@Path("/OmsLyraServiceSoapImpl")
public class OmsLyraServiceSoapImpl implements OmsLyraServiceSoap, MockInterface {
    private Set<String> liraLists = new HashSet<String>();

    private final ObjectMapper objectMapper = new ObjectMapper();

    private static HashMap<String, ArrayList<ObjectNode>> statRequests = new HashMap<>();
    private static HashMap<String, ArrayList<ObjectNode>> statResponses = new HashMap<>();

    private static MockResponse syncResponse = new MockResponse();
    private static MockResponse asyncResponse = new MockResponse();

    private static Integer responseNumSubmitLyraTask = 0;
    private static Integer responseNumLyraTaskStatusNotification = 0;

    private static Logger log = Logger.getLogger(OmsLyraServiceSoapImpl.class);

    public OmsLyraServiceSoapImpl() {
        liraLists.add("339");
        liraLists.add("500");
        liraLists.add("88");
        liraLists.add("305");
        liraLists.add("227");
    }

    public Error submitLyraTask(String componentId, String lyraTaskId, String orderId, String requestId, Parameters parameters) {
        String method = "submitLyraTask";

        log.debug("SubmitLyra start");

        Error error = generateResponses(method, responseNumSubmitLyraTask);

        //add request
        ObjectNode node = objectMapper.createObjectNode();
        node.put("componentId", componentId);
        node.put("lyraTaskId", lyraTaskId);
        //node.put("orderId", orderId);
        JsonNode jsonNode = objectMapper.valueToTree(parameters);
        node.set("parameters", jsonNode);
        addToStatisticMap(node, statRequests, method);

        //add response
        node = objectMapper.createObjectNode();
        jsonNode = objectMapper.valueToTree(error);
        node.set("error", jsonNode);
        addToStatisticMap(node, statResponses, method);

        if (liraLists.contains(lyraTaskId)) {
            log.debug(lyraTaskId);
            scheduleAsyncLTSN(componentId, orderId);

            responseNumLyraTaskStatusNotification = (responseNumLyraTaskStatusNotification + 1) %
                    (asyncResponse.getStatSyncResponsesCountByMethod("lyraTaskStatusNotification"));
        }
        if (lyraTaskId.equals("604")) {
            log.debug(lyraTaskId);
            scheduleAsyncGOS(componentId);
        }

        responseNumSubmitLyraTask = (responseNumSubmitLyraTask + 1) %
                (syncResponse.getStatSyncResponsesCountByMethod(method));

        return error;
    }

    private void scheduleAsyncLTSN(String componentId, String orderId) {
        LyraTaskStatusNotificationRequest lyraRequest = null;
        try {
            lyraRequest = objectMapper.treeToValue(asyncResponse.getStatSyncResponse("lyraTaskStatusNotification",
                    responseNumLyraTaskStatusNotification).get("LyraTaskStatusNotificationRequest"),
                    LyraTaskStatusNotificationRequest.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        LyraCallback lyraCallback = new LyraCallbackImplLTSN(componentId, lyraRequest.getRequestId(),
                orderId, lyraRequest.getLyraTaskId(), lyraRequest.getResultCode(),
                lyraRequest.getResultText(), lyraRequest.getParameters());
        Executor.schedule(lyraCallback, Long.valueOf(asyncResponse.getStatSyncResponse("lyraTaskStatusNotification",
                responseNumLyraTaskStatusNotification).get("time").asText()));
    }

    private void scheduleAsyncGOS(String componentId) {
        GetOrderStatusRequest lyraRequest = null;
        try {
            lyraRequest = objectMapper.treeToValue(asyncResponse.getStatSyncResponse("getOrderStatus",
                    responseNumLyraTaskStatusNotification).get("GetOrderStatusRequest"), GetOrderStatusRequest.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        LyraCallback lyraCallback = new LyraCallbackImplGOS(componentId, lyraRequest.getOrderId());
        Executor.schedule(lyraCallback, Long.valueOf(asyncResponse.getStatSyncResponse("getOrderStatus",
                responseNumLyraTaskStatusNotification).get("time").asText()));
    }

    private Error generateResponses(String method, Integer num) {
        Error error = new Error();
        String group = syncResponse.getStatSyncResponse(method, num).get("group").asText();
        String code = syncResponse.getStatSyncResponse(method, num).get("code").asText();
        String description = syncResponse.getStatSyncResponse(method, num).get("description").asText();
        String systemId = syncResponse.getStatSyncResponse(method, num).get("systemId").asText();
        String comment = syncResponse.getStatSyncResponse(method, num).get("comment").asText();

        error.setGroup(group);
        error.setCode(code);
        error.setDescription(description);
        error.setSystemId(systemId);
        error.setComment(comment);

        ObjectNode node = objectMapper.createObjectNode();
        node.put("group", group);
        node.put("code", code);
        node.put("description", description);
        node.put("systemId", systemId);
        node.put("comment", comment);
        addToStatisticMap(node, statResponses, method);

        return error;
    }

    // WARN! Side effect
    private void addToStatisticMap(ObjectNode statEntry, HashMap<String, ArrayList<ObjectNode>> statMap, String methodName) {
        ArrayList<ObjectNode> objectNodes = statMap.get(methodName);
        if (objectNodes == null) {
            objectNodes = new ArrayList<>();
        }
        objectNodes.add(statEntry);
        statMap.put(methodName, objectNodes);
    }

    @Deprecated
    public Error orderNotification(String orderId, String componentId, String status, Components components, Parameters parameters, Boolean jeopardy) {
        return null;
    }

    @Override
    public void setResponse(SetResponseRequest setResponseRequest) {
        responseNumSubmitLyraTask = 0;
        syncResponse = new MockResponse();
        statResponses = new HashMap<>();
        statRequests = new HashMap<>();
        syncResponse.setStatResponses(setResponseRequest);
    }

    @Override
    public void setAsync(SetResponseRequest setResponseRequest) {
        responseNumLyraTaskStatusNotification = 0;
        asyncResponse = new MockResponse();
        asyncResponse.setStatResponses(setResponseRequest);
        scheduleAsyncSO();
        scheduleAsyncUOS();
    }

    private void scheduleAsyncSO() {
        String method = "submitOrder";
        for (int i = 0; i < asyncResponse.getStatSyncResponsesCountByMethod(method); i++) {
            SubmitOrderRequest lyraRequest = null;
            try {
                lyraRequest = objectMapper.treeToValue(asyncResponse.getStatSyncResponse(method,
                        i).get("SubmitOrderRequest"), SubmitOrderRequest.class);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

            LyraCallback lyraCallback = new LyraCallbackImplSO(lyraRequest.getComponents(), lyraRequest.getCommonData());
            Executor.schedule(lyraCallback, Long.valueOf(asyncResponse.getStatSyncResponse(method,
                    i).get("time").asText()));
        }
    }

    private void scheduleAsyncUOS() {
        String method = "updateOrderStatus";
        for (int i = 0; i < asyncResponse.getStatSyncResponsesCountByMethod(method); i++) {
            UpdateOrderStatusRequest lyraRequest = null;
            try {
                lyraRequest = objectMapper.treeToValue(asyncResponse.getStatSyncResponse(method,
                        i).get("UpdateOrderStatusRequest"), UpdateOrderStatusRequest.class);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

            LyraCallback lyraCallback = new LyraCallbackImplUOS(lyraRequest.getComponentId(), lyraRequest.getOrderId(),
                    lyraRequest.getOperation(), lyraRequest.getParameters());
            Executor.schedule(lyraCallback, Long.valueOf(asyncResponse.getStatSyncResponse(method,
                    i).get("time").asText()));
        }
    }

    @Override
    public AssertInfo getAssertInfo() {
        AssertInfo assertInfo = new AssertInfo();
        assertInfo.setResponses(new HashMap<>(statResponses));
        assertInfo.setRequests(new HashMap<>(statRequests));
        statResponses.clear();
        statRequests.clear();
        return assertInfo;
    }
}
