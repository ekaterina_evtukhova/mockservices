package rt.deltasolutions.om.rt.lyra.webservice;

/**
 * Created by Katy on 01.04.2016.
 */
public interface LyraCallback extends Runnable {
    public void sendAsyncResponse();
}
