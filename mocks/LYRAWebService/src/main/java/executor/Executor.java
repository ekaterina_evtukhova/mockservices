package executor;

import rt.deltasolutions.om.rt.lyra.webservice.LyraCallback;
import rt.deltasolutions.om.rt.lyra.webservice.LyraCallbackImplLTSN;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by Katy on 23.03.2016.
 */
public class Executor {

    private static ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();

    public static void schedule(LyraCallback lyraCallback, Long time) {
        System.out.println("Schedule start");
//        LyraCallbackImplLTSN lyraCallback =
//                new LyraCallbackImplLTSN(componentId);
        scheduledExecutorService.schedule(lyraCallback, time, TimeUnit.SECONDS);
    }

}
