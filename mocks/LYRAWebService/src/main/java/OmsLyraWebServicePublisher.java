import rt.deltasolutions.om.rt.lyra.webservice.OmsLyraServiceSoapImpl;

import javax.xml.ws.Endpoint;

/**
 * Created by Katy on 11.03.2016.
 */
public class OmsLyraWebServicePublisher {
    public static void main(String... args) {
        Endpoint.publish("http://127.0.0.1:24081/mockOmsLyraService", new OmsLyraServiceSoapImpl());
    }
}
