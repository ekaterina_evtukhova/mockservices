package parameters;

import ru.deltasolutions.om.rt.lyra.service.Parameters;

/**
 * Created by Katy on 23.03.2016.
 */
public class AsyncParameters {
    public String getLyraTaskId() {
        return lyraTaskId;
    }

    public void setLyraTaskId(String lyraTaskId) {
        this.lyraTaskId = lyraTaskId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public ru.deltasolutions.om.rt.lyra.service.Parameters getParameters() {
        return parameters;
    }

    public void setParameters(Parameters parameters) {
        this.parameters = parameters;
    }

    private String lyraTaskId;
    private String orderId;
    private String requestId;
    private ru.deltasolutions.om.rt.lyra.service.Parameters parameters;

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    private String operation;

    public AsyncParameters(String lyraTaskId, String orderId, String requestId,
                           ru.deltasolutions.om.rt.lyra.service.Parameters parameters, String operation) {
        this.lyraTaskId = lyraTaskId;
        this.orderId = orderId;
        this.requestId = requestId;
        this.parameters = parameters;
        this.operation = operation;
    }
}
