package parameters;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Katy on 23.03.2016.
 */

public class RequestParameters {
    private static Map<String, AsyncParameters> parameters = new HashMap<String, AsyncParameters>();
    private static volatile RequestParameters instance;

    public static RequestParameters getInstance() {
        RequestParameters localInstance = instance;
        if (localInstance == null) {
            synchronized (RequestParameters.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new RequestParameters();
                }
            }
        }
        return localInstance;
    }

    public void setParameters(String componentId, AsyncParameters parameters) {
        this.parameters.put(componentId, parameters);
    }

    public AsyncParameters getRequestParametersByComponentId(String componentId) {
        return parameters.get(componentId);
    }

    public void deleteParametersByComponentId(String componentId) {
        parameters.remove(componentId);
    }
}
