
package ru.deltasolutions.om.rt.lyra.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="orderNotificationResult" type="{http://www.deltasolutions.ru/om/rt/lyra/service}Error"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "orderNotificationResult"
})
@XmlRootElement(name = "OrderNotificationResponse")
public class OrderNotificationResponse {

    @XmlElement(required = true)
    protected Error orderNotificationResult;

    /**
     * Gets the value of the orderNotificationResult property.
     * 
     * @return
     *     possible object is
     *     {@link Error }
     *     
     */
    public Error getOrderNotificationResult() {
        return orderNotificationResult;
    }

    /**
     * Sets the value of the orderNotificationResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link Error }
     *     
     */
    public void setOrderNotificationResult(Error value) {
        this.orderNotificationResult = value;
    }

}
