
package ru.deltasolutions.om.rt.lyra.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Notification complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Notification"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="resourceInfo" type="{http://www.deltasolutions.ru/om/rt/lyra/service}tResourceInfo"/&gt;
 *         &lt;element name="installed" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Notification", propOrder = {
    "resourceInfo",
    "installed"
})
public class Notification {

    @XmlElement(required = true)
    protected TResourceInfo resourceInfo;
    protected int installed;

    /**
     * Gets the value of the resourceInfo property.
     * 
     * @return
     *     possible object is
     *     {@link TResourceInfo }
     *     
     */
    public TResourceInfo getResourceInfo() {
        return resourceInfo;
    }

    /**
     * Sets the value of the resourceInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TResourceInfo }
     *     
     */
    public void setResourceInfo(TResourceInfo value) {
        this.resourceInfo = value;
    }

    /**
     * Gets the value of the installed property.
     * 
     */
    public int getInstalled() {
        return installed;
    }

    /**
     * Sets the value of the installed property.
     * 
     */
    public void setInstalled(int value) {
        this.installed = value;
    }

}
