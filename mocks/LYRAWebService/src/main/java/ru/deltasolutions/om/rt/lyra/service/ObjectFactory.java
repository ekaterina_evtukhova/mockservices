
package ru.deltasolutions.om.rt.lyra.service;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.deltasolutions.om.rt.lyra.service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.deltasolutions.om.rt.lyra.service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OrderNotification }
     * 
     */
    public OrderNotification createOrderNotification() {
        return new OrderNotification();
    }

    /**
     * Create an instance of {@link Components }
     * 
     */
    public Components createComponents() {
        return new Components();
    }

    /**
     * Create an instance of {@link Parameters }
     * 
     */
    public Parameters createParameters() {
        return new Parameters();
    }

    /**
     * Create an instance of {@link SubmitLyraTaskResponse }
     * 
     */
    public SubmitLyraTaskResponse createSubmitLyraTaskResponse() {
        return new SubmitLyraTaskResponse();
    }

    /**
     * Create an instance of {@link Error }
     * 
     */
    public Error createError() {
        return new Error();
    }

    /**
     * Create an instance of {@link OrderNotificationResponse }
     * 
     */
    public OrderNotificationResponse createOrderNotificationResponse() {
        return new OrderNotificationResponse();
    }

    /**
     * Create an instance of {@link SubmitLyraTask }
     * 
     */
    public SubmitLyraTask createSubmitLyraTask() {
        return new SubmitLyraTask();
    }

    /**
     * Create an instance of {@link IVendor }
     * 
     */
    public IVendor createIVendor() {
        return new IVendor();
    }

    /**
     * Create an instance of {@link IResourceCategory }
     * 
     */
    public IResourceCategory createIResourceCategory() {
        return new IResourceCategory();
    }

    /**
     * Create an instance of {@link TaskEntityList }
     * 
     */
    public TaskEntityList createTaskEntityList() {
        return new TaskEntityList();
    }

    /**
     * Create an instance of {@link ITransferTerms }
     * 
     */
    public ITransferTerms createITransferTerms() {
        return new ITransferTerms();
    }

    /**
     * Create an instance of {@link Notification }
     * 
     */
    public Notification createNotification() {
        return new Notification();
    }

    /**
     * Create an instance of {@link ITransferType }
     * 
     */
    public ITransferType createITransferType() {
        return new ITransferType();
    }

    /**
     * Create an instance of {@link TaskEntity }
     * 
     */
    public TaskEntity createTaskEntity() {
        return new TaskEntity();
    }

    /**
     * Create an instance of {@link ITransferReason }
     * 
     */
    public ITransferReason createITransferReason() {
        return new ITransferReason();
    }

    /**
     * Create an instance of {@link Worker }
     * 
     */
    public Worker createWorker() {
        return new Worker();
    }

    /**
     * Create an instance of {@link ResourceRequirement }
     * 
     */
    public ResourceRequirement createResourceRequirement() {
        return new ResourceRequirement();
    }

    /**
     * Create an instance of {@link TClient }
     * 
     */
    public TClient createTClient() {
        return new TClient();
    }

    /**
     * Create an instance of {@link TResourceInfo }
     * 
     */
    public TResourceInfo createTResourceInfo() {
        return new TResourceInfo();
    }

    /**
     * Create an instance of {@link IAccount }
     * 
     */
    public IAccount createIAccount() {
        return new IAccount();
    }

    /**
     * Create an instance of {@link IAccountScope }
     * 
     */
    public IAccountScope createIAccountScope() {
        return new IAccountScope();
    }

    /**
     * Create an instance of {@link IResourceType }
     * 
     */
    public IResourceType createIResourceType() {
        return new IResourceType();
    }

    /**
     * Create an instance of {@link Component }
     * 
     */
    public Component createComponent() {
        return new Component();
    }

    /**
     * Create an instance of {@link Parameter }
     * 
     */
    public Parameter createParameter() {
        return new Parameter();
    }

    /**
     * Create an instance of {@link ResourceMovementNotification }
     * 
     */
    public ResourceMovementNotification createResourceMovementNotification() {
        return new ResourceMovementNotification();
    }

    /**
     * Create an instance of {@link IResource }
     * 
     */
    public IResource createIResource() {
        return new IResource();
    }

}
