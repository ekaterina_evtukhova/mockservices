
package ru.deltasolutions.om.rt.lyra.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Component complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Component"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="serviceId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="serviceName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="parentServiceName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="parentServiceId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="parameters" type="{http://www.deltasolutions.ru/om/rt/lyra/service}AsyncParameters" minOccurs="0"/&gt;
 *         &lt;element name="Jeopardy" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="taskEntityList" type="{http://www.deltasolutions.ru/om/rt/lyra/service}taskEntityList" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Component", propOrder = {
    "serviceId",
    "serviceName",
    "parentServiceName",
    "parentServiceId",
    "parameters",
    "jeopardy",
    "taskEntityList"
})
public class Component {

    protected Integer serviceId;
    protected String serviceName;
    protected String parentServiceName;
    protected Integer parentServiceId;
    protected Parameters parameters;
    @XmlElement(name = "Jeopardy")
    protected Boolean jeopardy;
    protected TaskEntityList taskEntityList;

    /**
     * Gets the value of the serviceId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getServiceId() {
        return serviceId;
    }

    /**
     * Sets the value of the serviceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setServiceId(Integer value) {
        this.serviceId = value;
    }

    /**
     * Gets the value of the serviceName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * Sets the value of the serviceName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceName(String value) {
        this.serviceName = value;
    }

    /**
     * Gets the value of the parentServiceName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentServiceName() {
        return parentServiceName;
    }

    /**
     * Sets the value of the parentServiceName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentServiceName(String value) {
        this.parentServiceName = value;
    }

    /**
     * Gets the value of the parentServiceId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getParentServiceId() {
        return parentServiceId;
    }

    /**
     * Sets the value of the parentServiceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setParentServiceId(Integer value) {
        this.parentServiceId = value;
    }

    /**
     * Gets the value of the parameters property.
     * 
     * @return
     *     possible object is
     *     {@link Parameters }
     *     
     */
    public Parameters getParameters() {
        return parameters;
    }

    /**
     * Sets the value of the parameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link Parameters }
     *     
     */
    public void setParameters(Parameters value) {
        this.parameters = value;
    }

    /**
     * Gets the value of the jeopardy property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isJeopardy() {
        return jeopardy;
    }

    /**
     * Sets the value of the jeopardy property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setJeopardy(Boolean value) {
        this.jeopardy = value;
    }

    /**
     * Gets the value of the taskEntityList property.
     * 
     * @return
     *     possible object is
     *     {@link TaskEntityList }
     *     
     */
    public TaskEntityList getTaskEntityList() {
        return taskEntityList;
    }

    /**
     * Sets the value of the taskEntityList property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaskEntityList }
     *     
     */
    public void setTaskEntityList(TaskEntityList value) {
        this.taskEntityList = value;
    }

}
