
package ru.deltasolutions.om.rt.lyraomsservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.deltasolutions.om.rt.lyraomsservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetOrderStatusRequest_QNAME = new QName("http://www.deltasolutions.ru/om/rt/LyraOmsService/", "GetOrderStatusRequest");
    private final static QName _LyraTaskStatusNotificationResponse_QNAME = new QName("http://www.deltasolutions.ru/om/rt/LyraOmsService/", "LyraTaskStatusNotificationResponse");
    private final static QName _UpdateOrderStatusResponse_QNAME = new QName("http://www.deltasolutions.ru/om/rt/LyraOmsService/", "UpdateOrderStatusResponse");
    private final static QName _LyraTaskStatusNotificationRequest_QNAME = new QName("http://www.deltasolutions.ru/om/rt/LyraOmsService/", "LyraTaskStatusNotificationRequest");
    private final static QName _SubmitOrderRequest_QNAME = new QName("http://www.deltasolutions.ru/om/rt/LyraOmsService/", "SubmitOrderRequest");
    private final static QName _GetOrderStatusResponse_QNAME = new QName("http://www.deltasolutions.ru/om/rt/LyraOmsService/", "GetOrderStatusResponse");
    private final static QName _UpdateOrderStatusRequest_QNAME = new QName("http://www.deltasolutions.ru/om/rt/LyraOmsService/", "UpdateOrderStatusRequest");
    private final static QName _SubmitOrderResponse_QNAME = new QName("http://www.deltasolutions.ru/om/rt/LyraOmsService/", "SubmitOrderResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.deltasolutions.om.rt.lyraomsservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link LyraTaskStatusNotificationRequest }
     * 
     */
    public LyraTaskStatusNotificationRequest createLyraTaskStatusNotificationRequest() {
        return new LyraTaskStatusNotificationRequest();
    }

    /**
     * Create an instance of {@link UpdateOrderStatusResponse }
     * 
     */
    public UpdateOrderStatusResponse createUpdateOrderStatusResponse() {
        return new UpdateOrderStatusResponse();
    }

    /**
     * Create an instance of {@link LyraTaskStatusNotificationResponse }
     * 
     */
    public LyraTaskStatusNotificationResponse createLyraTaskStatusNotificationResponse() {
        return new LyraTaskStatusNotificationResponse();
    }

    /**
     * Create an instance of {@link GetOrderStatusRequest }
     * 
     */
    public GetOrderStatusRequest createGetOrderStatusRequest() {
        return new GetOrderStatusRequest();
    }

    /**
     * Create an instance of {@link SubmitOrderResponse }
     * 
     */
    public SubmitOrderResponse createSubmitOrderResponse() {
        return new SubmitOrderResponse();
    }

    /**
     * Create an instance of {@link GetOrderStatusResponse }
     * 
     */
    public GetOrderStatusResponse createGetOrderStatusResponse() {
        return new GetOrderStatusResponse();
    }

    /**
     * Create an instance of {@link UpdateOrderStatusRequest }
     * 
     */
    public UpdateOrderStatusRequest createUpdateOrderStatusRequest() {
        return new UpdateOrderStatusRequest();
    }

    /**
     * Create an instance of {@link SubmitOrderRequest }
     * 
     */
    public SubmitOrderRequest createSubmitOrderRequest() {
        return new SubmitOrderRequest();
    }

    /**
     * Create an instance of {@link Parameters }
     * 
     */
    public Parameters createParameters() {
        return new Parameters();
    }

    /**
     * Create an instance of {@link Attributes }
     * 
     */
    public Attributes createAttributes() {
        return new Attributes();
    }

    /**
     * Create an instance of {@link Components }
     * 
     */
    public Components createComponents() {
        return new Components();
    }

    /**
     * Create an instance of {@link TComponents }
     * 
     */
    public TComponents createTComponents() {
        return new TComponents();
    }

    /**
     * Create an instance of {@link Error }
     * 
     */
    public Error createError() {
        return new Error();
    }

    /**
     * Create an instance of {@link TaskEntity }
     * 
     */
    public TaskEntity createTaskEntity() {
        return new TaskEntity();
    }

    /**
     * Create an instance of {@link Worker }
     * 
     */
    public Worker createWorker() {
        return new Worker();
    }

    /**
     * Create an instance of {@link TComponent }
     * 
     */
    public TComponent createTComponent() {
        return new TComponent();
    }

    /**
     * Create an instance of {@link Attribute }
     * 
     */
    public Attribute createAttribute() {
        return new Attribute();
    }

    /**
     * Create an instance of {@link TaskEntityList }
     * 
     */
    public TaskEntityList createTaskEntityList() {
        return new TaskEntityList();
    }

    /**
     * Create an instance of {@link CommonData }
     * 
     */
    public CommonData createCommonData() {
        return new CommonData();
    }

    /**
     * Create an instance of {@link Component }
     * 
     */
    public Component createComponent() {
        return new Component();
    }

    /**
     * Create an instance of {@link Parameter }
     * 
     */
    public Parameter createParameter() {
        return new Parameter();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOrderStatusRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.deltasolutions.ru/om/rt/LyraOmsService/", name = "GetOrderStatusRequest")
    public JAXBElement<GetOrderStatusRequest> createGetOrderStatusRequest(GetOrderStatusRequest value) {
        return new JAXBElement<GetOrderStatusRequest>(_GetOrderStatusRequest_QNAME, GetOrderStatusRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LyraTaskStatusNotificationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.deltasolutions.ru/om/rt/LyraOmsService/", name = "LyraTaskStatusNotificationResponse")
    public JAXBElement<LyraTaskStatusNotificationResponse> createLyraTaskStatusNotificationResponse(LyraTaskStatusNotificationResponse value) {
        return new JAXBElement<LyraTaskStatusNotificationResponse>(_LyraTaskStatusNotificationResponse_QNAME, LyraTaskStatusNotificationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateOrderStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.deltasolutions.ru/om/rt/LyraOmsService/", name = "UpdateOrderStatusResponse")
    public JAXBElement<UpdateOrderStatusResponse> createUpdateOrderStatusResponse(UpdateOrderStatusResponse value) {
        return new JAXBElement<UpdateOrderStatusResponse>(_UpdateOrderStatusResponse_QNAME, UpdateOrderStatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LyraTaskStatusNotificationRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.deltasolutions.ru/om/rt/LyraOmsService/", name = "LyraTaskStatusNotificationRequest")
    public JAXBElement<LyraTaskStatusNotificationRequest> createLyraTaskStatusNotificationRequest(LyraTaskStatusNotificationRequest value) {
        return new JAXBElement<LyraTaskStatusNotificationRequest>(_LyraTaskStatusNotificationRequest_QNAME, LyraTaskStatusNotificationRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitOrderRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.deltasolutions.ru/om/rt/LyraOmsService/", name = "SubmitOrderRequest")
    public JAXBElement<SubmitOrderRequest> createSubmitOrderRequest(SubmitOrderRequest value) {
        return new JAXBElement<SubmitOrderRequest>(_SubmitOrderRequest_QNAME, SubmitOrderRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOrderStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.deltasolutions.ru/om/rt/LyraOmsService/", name = "GetOrderStatusResponse")
    public JAXBElement<GetOrderStatusResponse> createGetOrderStatusResponse(GetOrderStatusResponse value) {
        return new JAXBElement<GetOrderStatusResponse>(_GetOrderStatusResponse_QNAME, GetOrderStatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateOrderStatusRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.deltasolutions.ru/om/rt/LyraOmsService/", name = "UpdateOrderStatusRequest")
    public JAXBElement<UpdateOrderStatusRequest> createUpdateOrderStatusRequest(UpdateOrderStatusRequest value) {
        return new JAXBElement<UpdateOrderStatusRequest>(_UpdateOrderStatusRequest_QNAME, UpdateOrderStatusRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitOrderResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.deltasolutions.ru/om/rt/LyraOmsService/", name = "SubmitOrderResponse")
    public JAXBElement<SubmitOrderResponse> createSubmitOrderResponse(SubmitOrderResponse value) {
        return new JAXBElement<SubmitOrderResponse>(_SubmitOrderResponse_QNAME, SubmitOrderResponse.class, null, value);
    }

}
