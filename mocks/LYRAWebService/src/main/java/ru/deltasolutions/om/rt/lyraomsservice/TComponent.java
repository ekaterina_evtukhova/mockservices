
package ru.deltasolutions.om.rt.lyraomsservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TComponent complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TComponent">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="serviceId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="serviceName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="parentServiceName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="parentServiceId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="AsyncParameters" type="{http://www.deltasolutions.ru/om/rt/LyraOmsService/}AsyncParameters" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Jeopardy" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="taskEntityList" type="{http://www.deltasolutions.ru/om/rt/LyraOmsService/}TaskEntityList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TComponent", propOrder = {
    "serviceId",
    "serviceName",
    "parentServiceName",
    "parentServiceId",
    "parameters",
    "status",
    "jeopardy",
    "taskEntityList"
})
public class TComponent {

    protected Integer serviceId;
    protected String serviceName;
    protected String parentServiceName;
    protected Integer parentServiceId;
    @XmlElement(name = "AsyncParameters")
    protected Parameters parameters;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "Jeopardy")
    protected Boolean jeopardy;
    protected TaskEntityList taskEntityList;

    /**
     * Gets the value of the serviceId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getServiceId() {
        return serviceId;
    }

    /**
     * Sets the value of the serviceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setServiceId(Integer value) {
        this.serviceId = value;
    }

    /**
     * Gets the value of the serviceName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * Sets the value of the serviceName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceName(String value) {
        this.serviceName = value;
    }

    /**
     * Gets the value of the parentServiceName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentServiceName() {
        return parentServiceName;
    }

    /**
     * Sets the value of the parentServiceName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentServiceName(String value) {
        this.parentServiceName = value;
    }

    /**
     * Gets the value of the parentServiceId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getParentServiceId() {
        return parentServiceId;
    }

    /**
     * Sets the value of the parentServiceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setParentServiceId(Integer value) {
        this.parentServiceId = value;
    }

    /**
     * Gets the value of the parameters property.
     * 
     * @return
     *     possible object is
     *     {@link Parameters }
     *     
     */
    public Parameters getParameters() {
        return parameters;
    }

    /**
     * Sets the value of the parameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link Parameters }
     *     
     */
    public void setParameters(Parameters value) {
        this.parameters = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the jeopardy property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isJeopardy() {
        return jeopardy;
    }

    /**
     * Sets the value of the jeopardy property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setJeopardy(Boolean value) {
        this.jeopardy = value;
    }

    /**
     * Gets the value of the taskEntityList property.
     * 
     * @return
     *     possible object is
     *     {@link TaskEntityList }
     *     
     */
    public TaskEntityList getTaskEntityList() {
        return taskEntityList;
    }

    /**
     * Sets the value of the taskEntityList property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaskEntityList }
     *     
     */
    public void setTaskEntityList(TaskEntityList value) {
        this.taskEntityList = value;
    }

}
