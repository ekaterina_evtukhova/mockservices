import org.junit.runner.RunWith;
import ru.deltasolutions.om.rt.lyra.service.*;
import ru.deltasolutions.om.rt.lyraomsservice.LyraOmsServicePortType;
import ru.deltasolutions.oms.smart.mocker.mocker.annotation.BloodyTest;
import ru.deltasolutions.oms.smart.mocker.mocker.runner.BloodyMockerTestRunner;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Katy on 18.04.2016.
 */
@RunWith(BloodyMockerTestRunner.class)
public class LyraTest {
    private Object lyraLock = new Object();
    private volatile OmsLyraServiceSoap omsLyraServiceSoap;

    @BloodyTest(pathToConfig = "lyraTestFile.json")
    public void lyraTest() {
        sendSubmitLyraTask();
    }

    @BloodyTest(pathToConfig = "lyraTestFile.json")
    public void lyraTest2() {

    }

    @BloodyTest(pathToConfig = "lyraTest1.json")
    public void lyraTest1() {

    }

    private void sendSubmitLyraTask() {
        String componentId = "66605254";
        String lyraTaskId = "604";
        String orderId = "23869";
        Parameters parameters = new Parameters();

        Parameter parameter = new Parameter();
        parameter.setName("code");
        parameter.setValue("303");
        parameters.addParameter(parameter);

        parameter = new Parameter();
        parameter.setName("description");
        parameter.setValue("При создании абонента АСР остались ошибочные заявки");
        parameters.addParameter(parameter);

        parameter = new Parameter();
        parameter.setName("system_id");
        parameter.setValue("OMS");
        parameters.addParameter(parameter);

        parameter = new Parameter();
        parameter.setName("task_name");
        parameter.setValue("BIS_CreateTestAbonent");
        parameters.addParameter(parameter);


        try {
            ru.deltasolutions.om.rt.lyra.service.Error error =
                    getOmsLyraServicePortType().submitLyraTask(componentId, lyraTaskId, orderId, "234567890", parameters);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private OmsLyraServiceSoap getOmsLyraServicePortType() throws MalformedURLException {
        if (omsLyraServiceSoap == null) {
            synchronized (lyraLock) {
                if (omsLyraServiceSoap == null) {
                    omsLyraServiceSoap = getLyraApi();
                }
            }
        }
        return omsLyraServiceSoap;
    }

    private OmsLyraServiceSoap getLyraApi() throws MalformedURLException {
        URL wsdlLocation = OmsLyraService.class
                .getResource("/META-INF/wsdl/OmsLyraService.wsdl");
        OmsLyraService service = new OmsLyraService(wsdlLocation);
        OmsLyraServiceSoap ret_service = service.getOmsLyraServiceSoap();
        BindingProvider bindingProvider = (BindingProvider) ret_service;
        bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
                "http://localhost:8080/LYRAWebService-1.0.0-SNAPSHOT/OmsLyraServiceSoapImpl");
        return (OmsLyraServiceSoap) bindingProvider;
    }
}
