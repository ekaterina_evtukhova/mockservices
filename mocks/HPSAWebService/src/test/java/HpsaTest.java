import org.junit.runner.RunWith;
import ru.deltasolutions.activator.rt.ws.*;
import ru.deltasolutions.oms.smart.mocker.mocker.annotation.BloodyTest;
import ru.deltasolutions.oms.smart.mocker.mocker.runner.BloodyMockerTestRunner;

import javax.xml.ws.BindingProvider;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Katy on 20.04.2016.
 */
@RunWith(BloodyMockerTestRunner.class)
public class HpsaTest {

    private Object hpsaLock = new Object();
    private volatile Activator activator;

    @BloodyTest(pathToConfig = "hpsaTestFile.json")
    public void hpsaTest() {
        sendPlaceOrder();
    }

    @BloodyTest(pathToConfig = "hpsaTestFile.json")
    public void hpsaTest2() {

    }

    private void sendPlaceOrder() {
        Order order = new Order();
        order.setRequestId("ertyui");
        order.setOrderId("23925");
        order.setOperation("Submit");

        Service service = new Service();
        service.setInstanceId("88590521");
        service.setSpecId("internet_access");
        service.setAction("Cease");
        List<Service> services = new ArrayList<Service>();
        services.add(service);

        order.setService(services);

        try {
            OrderState orderState =
                    getActivatorServicePortType().placeOrder(order);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private Activator getActivatorServicePortType() throws MalformedURLException {
        if (activator == null) {
            synchronized (hpsaLock) {
                if (activator == null) {
                    activator = getActivatorApi();
                }
            }
        }
        return activator;
    }

    private Activator getActivatorApi() throws MalformedURLException {
        URL wsdlLocation = Activator.class
                .getResource("/META-INF/wsdl/activator.wsdl");
        ActivatorServiceService service = new ActivatorServiceService(wsdlLocation);
        Activator ret_service = service.getActivatorPort();
        BindingProvider bindingProvider = (BindingProvider) ret_service;
        bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
                "http://localhost:8080/HPSAWebService-1.0.0-SNAPSHOT/ActivatorImpl");
        return (Activator) bindingProvider;
    }
}
