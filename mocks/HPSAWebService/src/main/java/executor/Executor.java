package executor;

import org.apache.log4j.Logger;
import rt.deltasolutions.om.rt.hpsa.webservice.ActivatorCallbackImpl;
import ru.deltasolutions.activator.rt.ws.Order;

import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by Katy on 18.03.2016.
 */
public class Executor {
    private static final ScheduledExecutorService scheduledExecutorService =
            Executors.newSingleThreadScheduledExecutor();
    private static Logger log = Logger.getLogger(Executor.class);

    public static void schedule(Order order, HashMap<String, String> params) {
        //log.debug("Start schedule\n");

        ActivatorCallbackImpl activatorCallback = new ActivatorCallbackImpl(order, params);
        Long time = Long.valueOf(params.get("time"));
        if (time > 0)
            scheduledExecutorService.schedule(activatorCallback, time, TimeUnit.SECONDS);
    }

}
