package ru.deltasolutions.activator.rt.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.deltasolutions.activator.rt.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Order_QNAME = new QName("http://ws.rt.activator.deltasolutions.ru/", "order");
    private final static QName _OrderState_QNAME = new QName("http://ws.rt.activator.deltasolutions.ru/", "orderState");
    private final static QName _Result_QNAME = new QName("http://ws.rt.activator.deltasolutions.ru/", "result");
    private final static QName _ParameterValue_QNAME = new QName("", "value");
    private final static QName _ServiceOperation_QNAME = new QName("", "operation");
    private final static QName _ServiceStateStatusText_QNAME = new QName("", "statusText");
    private final static QName _ResultResultText_QNAME = new QName("", "resultText");
    private final static QName _OrderScheduledTimestamp_QNAME = new QName("", "scheduledTimestamp");
    private final static QName _OrderCallbackEndpoint_QNAME = new QName("", "callbackEndpoint");
    private final static QName _OrderOriginator_QNAME = new QName("", "originator");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.deltasolutions.activator.rt.ws
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Order }
     *
     */
    public Order createOrder() {
        return new Order();
    }

    /**
     * Create an instance of {@link OrderState }
     *
     */
    public OrderState createOrderState() {
        return new OrderState();
    }

    /**
     * Create an instance of {@link Result }
     *
     */
    public Result createResult() {
        return new Result();
    }

    /**
     * Create an instance of {@link ServiceState }
     *
     */
    public ServiceState createServiceState() {
        return new ServiceState();
    }

    /**
     * Create an instance of {@link Service }
     *
     */
    public Service createService() {
        return new Service();
    }

    /**
     * Create an instance of {@link Parameter }
     *
     */
    public Parameter createParameter() {
        return new Parameter();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Order }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.rt.activator.deltasolutions.ru/", name = "order")
    public JAXBElement<Order> createOrder(Order value) {
        return new JAXBElement<Order>(_Order_QNAME, Order.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderState }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.rt.activator.deltasolutions.ru/", name = "orderState")
    public JAXBElement<OrderState> createOrderState(OrderState value) {
        return new JAXBElement<OrderState>(_OrderState_QNAME, OrderState.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Result }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.rt.activator.deltasolutions.ru/", name = "result")
    public JAXBElement<Result> createResult(Result value) {
        return new JAXBElement<Result>(_Result_QNAME, Result.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "value", scope = Parameter.class)
    public JAXBElement<String> createParameterValue(String value) {
        return new JAXBElement<String>(_ParameterValue_QNAME, String.class, Parameter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "operation", scope = Service.class)
    public JAXBElement<String> createServiceOperation(String value) {
        return new JAXBElement<String>(_ServiceOperation_QNAME, String.class, Service.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "statusText", scope = ServiceState.class)
    public JAXBElement<String> createServiceStateStatusText(String value) {
        return new JAXBElement<String>(_ServiceStateStatusText_QNAME, String.class, ServiceState.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "resultText", scope = Result.class)
    public JAXBElement<String> createResultResultText(String value) {
        return new JAXBElement<String>(_ResultResultText_QNAME, String.class, Result.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "scheduledTimestamp", scope = Order.class)
    public JAXBElement<XMLGregorianCalendar> createOrderScheduledTimestamp(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_OrderScheduledTimestamp_QNAME, XMLGregorianCalendar.class, Order.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "callbackEndpoint", scope = Order.class)
    public JAXBElement<String> createOrderCallbackEndpoint(String value) {
        return new JAXBElement<String>(_OrderCallbackEndpoint_QNAME, String.class, Order.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "originator", scope = Order.class)
    public JAXBElement<String> createOrderOriginator(String value) {
        return new JAXBElement<String>(_OrderOriginator_QNAME, String.class, Order.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "", name = "resultText", scope = OrderState.class)
    public JAXBElement<String> createOrderStateResultText(String value) {
        return new JAXBElement<String>(_ResultResultText_QNAME, String.class, OrderState.class, value);
    }

}