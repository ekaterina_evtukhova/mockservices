package rt.deltasolutions.om.rt.hpsa.webservice;

import ru.deltasolutions.activator.rt.ws.*;

import java.util.HashMap;

/**
 * Created by Katy on 16.03.2016.
 */
public class ActivatorCallbackImpl implements Runnable {
    private OrderState orderState;
    private HashMap<String, String> params;

    public ActivatorCallbackImpl(Order order, HashMap<String, String> params) {
        this.params = params;

        orderState = new OrderState(order);

        orderState.setResultCode(params.get("resultCode"));
        orderState.setResultText(params.get("resultText"));

        for (ServiceState serviceState : orderState.getServiceState()) {

            serviceState.setStatusCode(params.get("statusCode"));
            serviceState.setStatusText(params.get("statusText"));

            serviceState.setErrorCode(Long.valueOf(params.get("errorCode")));
            serviceState.setErrorText(params.get("errorText"));
        }
    }

    public void run() {
        System.out.println("Run notification with ");
        sendAsyncResponse();
    }

    private void sendAsyncResponse() {
        System.out.println("start asynch");
        ActivatorCallbackService activatorCallbackService = new ActivatorCallbackService();
        System.out.println("get port");
        ActivatorCallback activatorCallback = activatorCallbackService.getActivatorCallbackPort();
        System.out.println("send request");
        Result res = activatorCallback.notifyOrderState(orderState);
        System.out.println(res);
    }
}
