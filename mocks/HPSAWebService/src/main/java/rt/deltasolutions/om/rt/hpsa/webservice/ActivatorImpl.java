package rt.deltasolutions.om.rt.hpsa.webservice;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import context.MockResponse;
import executor.Executor;
import org.apache.log4j.Logger;
import ru.deltasolutions.activator.rt.ws.*;
import ru.deltasolutions.oms.smart.mocker.mockinterface.MockInterface;
import ru.deltasolutions.oms.smart.mocker.mockinterface.requests.SetResponseRequest;
import ru.deltasolutions.oms.smart.mocker.mockinterface.responses.AssertInfo;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.ws.rs.Path;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Katy on 11.03.2016.
 */
@WebService(endpointInterface = "ru.deltasolutions.activator.rt.ws.Activator")
@Path("/ActivatorImpl")
public class ActivatorImpl implements Activator, MockInterface {

    private final ObjectMapper objectMapper = new ObjectMapper();

    private static HashMap<String, ArrayList<ObjectNode>> statRequests = new HashMap<>();
    private static HashMap<String, ArrayList<ObjectNode>> statResponses = new HashMap<>();

    private static MockResponse syncResponse = new MockResponse();
    private static MockResponse asyncResponse = new MockResponse();

    private static Integer responseNumPlaceOrder = 0;
    private static Integer responseNumRetrieveOrderState = 0;
    private static Integer responseNumNotifyOrderState = 0;

    private static Logger log = Logger.getLogger(ActivatorImpl.class);

    @Deprecated
    public OrderOperations retrieveOrderOperations(Order order) {
        return null;
    }

    @WebMethod
    public OrderState placeOrder(Order order) {
        String method = "placeOrder";
        OrderState orderState = new OrderState(order);

        log.debug("Start PlaceOrder \n");

        setParameters(order);

        orderState.setResultCode(syncResponse.getStatSyncResponse(method,
                responseNumPlaceOrder).get("resultCode").asText());
        orderState.setResultText(syncResponse.getStatSyncResponse(method,
                responseNumPlaceOrder).get("resultText").asText());

        for (ServiceState serviceState : orderState.getServiceState()) {
            serviceState.setStatusCode(syncResponse.getStatSyncResponse(method,
                    responseNumPlaceOrder).get("statusCode").asText());

            serviceState.setStatusText(syncResponse.getStatSyncResponse(method,
                    responseNumPlaceOrder).get("statusText").asText());
        }

        //add request
        ObjectNode node = objectMapper.createObjectNode();
        JsonNode jsonNode = objectMapper.valueToTree(order);
        ((ObjectNode)jsonNode).remove("orderId");
        ((ObjectNode)jsonNode).remove("requestId");
        ((ObjectNode)jsonNode).remove("timestamp");
        ((ObjectNode)jsonNode).remove("scheduledTimestamp");
        node.set("order", jsonNode);
        addToStatisticMap(node, statRequests, method);

        //ass response
        node = objectMapper.createObjectNode();
        node.put("resultCode", orderState.getResultCode());
        node.put("resultText", orderState.getResultText());
        node.put("statusCode", ((ServiceState)orderState.getServiceState().toArray()[0]).getStatusCode());
        node.put("statusText", ((ServiceState)orderState.getServiceState().toArray()[0]).getStatusText());
        addToStatisticMap(node, statResponses, method);

        responseNumPlaceOrder = (responseNumPlaceOrder + 1) %
                (syncResponse.getStatSyncResponsesCountByMethod(method));

        return orderState;
    }

    // WARN! Side effect
    private void addToStatisticMap(ObjectNode statEntry, HashMap<String, ArrayList<ObjectNode>> statMap, String methodName) {
        ArrayList<ObjectNode> objectNodes = statMap.get(methodName);
        if (objectNodes == null) {
            objectNodes = new ArrayList<>();
        }
        objectNodes.add(statEntry);
        statMap.put(methodName, objectNodes);
    }

    private HashMap<String, String> parseError(String method, Integer num) {
        String statusCode = asyncResponse.getStatSyncResponse(method,
                num).get("statusCode").asText();
        String statusText = asyncResponse.getStatSyncResponse(method,
                num).get("statusText").asText();
        String resultCode = asyncResponse.getStatSyncResponse(method,
                num).get("resultCode").asText();
        String resultText = asyncResponse.getStatSyncResponse(method,
                num).get("resultText").asText();
        String errorCode = asyncResponse.getStatSyncResponse(method,
                num).get("errorCode").asText();
        String errorText = asyncResponse.getStatSyncResponse(method,
                num).get("errorText").asText();
        String time = asyncResponse.getStatSyncResponse(method,
                num).get("time").asText();

        HashMap<String, String> result = new HashMap<>();
        result.put("statusCode", statusCode);
        result.put("statusText", statusText);
        result.put("resultCode", resultCode);
        result.put("resultText", resultText);
        result.put("errorCode", errorCode);
        result.put("errorText", errorText);
        result.put("time", time);

        return result;
    }

    private void setParameters(Order order) {
        String method = "notifyOrderState";

        String serviceOperation = ((Service) (order.getService().toArray()[0])).getOperation();

        if (serviceOperation != null && serviceOperation.equals("Commit")) {
            log.debug("Set parameters Commit \n");
            Executor.schedule(order, parseError(method, responseNumNotifyOrderState));
            responseNumNotifyOrderState = (
                    responseNumNotifyOrderState + 1) % (asyncResponse.getStatSyncResponsesCountByMethod(method));
        } else {
            System.out.println("Set parameters Activate \n");
            Executor.schedule(order, parseError(method, responseNumNotifyOrderState));
            responseNumNotifyOrderState = (
                    responseNumNotifyOrderState + 1) % (asyncResponse.getStatSyncResponsesCountByMethod(method));
            Executor.schedule(order, parseError(method, responseNumNotifyOrderState));
            responseNumNotifyOrderState = (
                    responseNumNotifyOrderState + 1) % (asyncResponse.getStatSyncResponsesCountByMethod(method));
        }
    }

    @WebMethod
    public OrderState retrieveOrderState(Order order) {
        String method = "retrieveOrderState";
        OrderState orderState = new OrderState(order);

        orderState.setResultCode(syncResponse.getStatSyncResponse(method,
                responseNumRetrieveOrderState).get("resultCode").asText());
        orderState.setResultText(syncResponse.getStatSyncResponse(method,
                        responseNumRetrieveOrderState).get("resultText").asText());

        for (ServiceState serviceState: orderState.getServiceState() ) {
            serviceState.setStatusCode(syncResponse.getStatSyncResponse(method,
                    responseNumRetrieveOrderState).get("statusCode").asText());
            serviceState.setStatusText(syncResponse.getStatSyncResponse(method,
                            responseNumRetrieveOrderState).get("statusText").asText());
        }

        //add request
        ObjectNode node = objectMapper.createObjectNode();
        JsonNode jsonNode = objectMapper.valueToTree(order);
        node.set("order", jsonNode);
        addToStatisticMap(node, statRequests, method);

        //add response
        node = objectMapper.createObjectNode();
        jsonNode = objectMapper.valueToTree(orderState);
        node.set("orderState", jsonNode);
        addToStatisticMap(node, statResponses, method);

        responseNumRetrieveOrderState = (responseNumRetrieveOrderState + 1) %
                (syncResponse.getStatSyncResponsesCountByMethod(method));

        return orderState;
    }

    @Override
    public void setResponse(SetResponseRequest setResponseRequest) {
        syncResponse = new MockResponse();
        responseNumPlaceOrder = 0;
        responseNumRetrieveOrderState = 0;
        statResponses = new HashMap<>();
        statRequests = new HashMap<>();
        syncResponse.setStatResponses(setResponseRequest);
    }

    @Override
    public void setAsync(SetResponseRequest setResponseRequest) {
        asyncResponse = new MockResponse();
        responseNumNotifyOrderState = 0;
        asyncResponse.setStatResponses(setResponseRequest);
    }

    @Override
    public AssertInfo getAssertInfo() {
        AssertInfo assertInfo = new AssertInfo();
        assertInfo.setResponses(new HashMap<>(statResponses));
        assertInfo.setRequests(new HashMap<>(statRequests));
        statResponses.clear();
        statRequests.clear();
        return assertInfo;
    }
}
