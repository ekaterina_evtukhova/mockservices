package rt.deltasolutions.om.rt.hpsa.webservice;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Created by Katy on 20.04.2016.
 */
@ApplicationPath("/")
public class App  extends Application {
}
