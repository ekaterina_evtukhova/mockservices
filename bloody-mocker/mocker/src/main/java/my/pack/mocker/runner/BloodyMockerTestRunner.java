package my.pack.mocker.runner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import my.pack.mocker.annotation.BloodyTest;
import my.pack.mocker.config.MockConfig;
import my.pack.mocker.config.RunnerConfig;
import my.pack.mockinterface.requests.SetResponseRequest;
import my.pack.mockinterface.responses.AssertInfo;
import org.junit.internal.runners.model.EachTestNotifier;
import org.junit.runner.Description;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BloodyMockerTestRunner extends BlockJUnit4ClassRunner {

    private final ObjectMapper objectMapper = new ObjectMapper();

    public BloodyMockerTestRunner(Class<?> klass) throws InitializationError {
        super(klass);
    }

    @Override
    protected void runChild(final FrameworkMethod method, RunNotifier notifier) {
        BloodyTest annotation = method.getAnnotation(BloodyTest.class);
        String path = annotation.pathToConfig();
        RunnerConfig config = null;
        try {
            config = createConfig(path);
            configureMocks(config);
        } catch (IOException | URISyntaxException e) {
            throw new RuntimeException(e);
        }
        super.runChild(method, notifier);
        List<MockConfig> mocks = config.getMocks();
        mocks.forEach(mock -> assertRequestsAndResponses(mock, method, notifier));
    }

    private void assertRequestsAndResponses(MockConfig mock, FrameworkMethod method, RunNotifier notifier) {
        AssertInfo assertInfo = mock.getAssertInfo();
        HashMap<String, List<ObjectNode>> requests = assertInfo.getRequests();
        RestTemplate rt = new RestTemplate();
        AssertInfo asserts = rt.getForObject(mock.getAddress() + "/getAssertInfo", AssertInfo.class);
        HashMap<String, List<ObjectNode>> requestsFromMock = asserts.getRequests();
        if (!requests.equals(requestsFromMock)) {
            Description description = describeChild(method);
            EachTestNotifier eachTestNotifier = new EachTestNotifier(notifier, description);
            Exception exception = new Exception("Requests is different, expected: " + requests + ", actual" + requestsFromMock);
            eachTestNotifier.addFailure(exception);
            eachTestNotifier.fireTestFinished();
        }
    }

    private RunnerConfig createConfig(String path) throws URISyntaxException, IOException {
        URI uri = getClass().getClassLoader().getResource(path).toURI();
        byte[] bytes = Files.readAllBytes(Paths.get(uri));
        String mockConfigJson = new String(bytes);
        RunnerConfig config = objectMapper.readValue(mockConfigJson, RunnerConfig.class);
        return config;
    }

    private void configureMocks(RunnerConfig config) throws IOException, URISyntaxException {
        RestTemplate rt = new RestTemplate();
        config.getMocks().stream().forEach(mock -> {
            configureMockMethod(rt, mock);
        });
    }

    private void configureMockMethod(RestTemplate rt, MockConfig mock) {
        String address = mock.getAddress();
        SetResponseRequest setResponseRequest = new SetResponseRequest();
        Map<String, ObjectNode> methodsResponses = mock.getMethodsResponses();
        methodsResponses.entrySet().stream().forEach(en -> {
            configureMethodResponses(rt, address, setResponseRequest, en);
        });
        Map<String, ObjectNode> asyncRequests = mock.getAsyncRequests();
        if (asyncRequests != null) {
            rt.postForObject(address + "/configureAsync", asyncRequests, Void.class);
        }
    }

    private void configureMethodResponses(RestTemplate rt, String address, SetResponseRequest setResponseRequest, Map.Entry<String, ObjectNode> en) {
        String methodName = en.getKey();
        ObjectNode value = en.getValue();
        setResponseRequest.setMethodName(methodName);
        try {
            String s = objectMapper.writeValueAsString(value);
            setResponseRequest.setJsonResponse(s);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        rt.postForObject(address + "/configureResponse", setResponseRequest, Void.class);
    }

    @Override
    protected List<FrameworkMethod> computeTestMethods() {
        return getTestClass().getAnnotatedMethods(BloodyTest.class);
    }
}
