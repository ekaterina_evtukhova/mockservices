package my.pack.mocker.config;

import lombok.Data;

import java.util.List;

@Data
public class RunnerConfig {
    private List<MockConfig> mocks;
}
