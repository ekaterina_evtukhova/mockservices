package my.pack.mocker.runner;

import my.pack.mocker.annotation.BloodyTest;
import org.junit.runner.RunWith;

@RunWith(BloodyMockerTestRunner.class)
public class BloodyMockerRunWithTest {

    @BloodyTest(pathToConfig = "testFile.json")
    public void badTest() {
//        String url = "http://localhost:8080/rs-mock/restTest";
//        RestTemplate rt = new RestTemplate();
//        TestServiceRequest request = new TestServiceRequest();
//        request.setB(true);
//        request.setS("IIIIHHH");
//        ResponseEntity<Response> responseResponseEntity = rt.postForEntity(url, request, Response.class);
//        System.out.println(responseResponseEntity.getBody());
    }

    @BloodyTest(pathToConfig = "testFile.json")
    public void goodTest() {
//        String url = "http://localhost:8080/rs-mock/restTest";
//        RestTemplate rt = new RestTemplate();
//        TestServiceRequest request = new TestServiceRequest();
//        request.setB(false);
//        request.setS("IIIIHHH");
//        ResponseEntity<Response> responseResponseEntity = rt.postForEntity(url, request, Response.class);
//        System.out.println(responseResponseEntity.getBody());
    }

}
;
