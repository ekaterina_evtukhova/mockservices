package my.pack.rsmock.asynctasks;

import lombok.Data;

@Data
public class SecondTask implements Runnable {

    private int age;
    private boolean condition;
    private int time;

    @Override
    public void run() {
        System.out.println("Second task: " + this);
    }
}
